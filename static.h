//
//  static.h
//  staticdemo
//
//  Created by WuDongyang on 14-10-24.
//  Copyright (c) 2014年 WuDongyang. All rights reserved.
//

#ifndef staticdemo_static_h
#define staticdemo_static_h

//返回值
typedef enum _RT_CODE
{
    AV_SUCCESS              = 0,   // 成功
    AV_UNKOWN               = -1,  // 未知错误
    AV_TIMEOUT              = -2,  // 超时
    AV_PARAMS_ERROR         = -3,  // 参数错误
    AV_PARSE_ERROR          = -4,  //地址无效
    AV_STRING_CATCH         = -5,  //字符串处理异常
    AV_STRING_ERROR         = -6,  //字符串处理错误
    AV_NO_MATCH_ADD         = -7,  //未找到解析规则
    AV_RULE_INIT_ERROR      = -8,  //规则加载失败
    AV_VERSION_ERROR        = -9,  //对应的规则版本不匹配
    AV_VERSION_MALFORMAT    = -10, //版本号格式不正确
    AV_REGISTFUN_ERROR      = -11, //注册回调函数失败
    AV_SETLOGLEVEL_ERROR    = -12, //设置日志级别失败
    AV_SET_UA_ERROR         = -13, //设置UA失败
} RT_CODE;


typedef const wchar_t* LPCTSTR;
typedef const char* LPCSTR;
typedef const char* LPCSTR;
typedef char* LPSTR;
// typedef bool BOOL;
typedef long long __int64;



/**
 * Morefun解析规则
 **/
RT_CODE AnalysisRealUrlForMorefun(const int sourceId,const LPCSTR url,char* realURL, int len, const LPCSTR ua,const LPCSTR sourceKind);

RT_CODE AnalysisRealUrlForMorefun2(const int sourceId,const LPCSTR url,
                                              char* realURL, int len, const LPCSTR ua,const LPCSTR sourceKind,
                                              const LPCSTR spiderName,const LPCSTR videoParam);

/**
 * 原型：bool URLTIsRunning()
 * 功能：获取当前运行状态
 **/
bool URLTIsRunning();

#endif
