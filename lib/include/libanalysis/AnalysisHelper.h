/*
 * 2011-11-16  闫佳峰：实现视频地址解析
 * 2011-12-20  赵春波：合并解析功能
 * 2012-04-18  赵春波：增加跳转解析功能
 */
#ifndef __ANALYSISHELPER_H__
#define __ANALYSISHELPER_H__
#define _CRTDBG_MAP_ALLOC 

#include "base.h"

#ifdef __cplusplus
extern "C" {
#endif


#define GET_ARRAY_LEN(array,len){len=(sizeof(array)/sizeof(array[0]));}

//////////////////////////////////////////////////////////////////////////
/**
 * 原型：string getFileIDMixString(string seed);
 * 功能：优酷处理函数-根据seed获取filed
 * 参数：seed 优酷提供
 * 返回值：字符串
 **/
//string getFileIDMixString(string seed);

/**
 * 原型：string Format(string format,string chars)
 * 功能：组合字符串
 * 参数：format 组合格式
 *       chars 字符串
 * 返回值：符合条件的字符串
 **/
//string Format(string format, string chars);

/**
 * 原型：string Replace(string base,string target,string source)
 * 功能：替换字符串
 * 参数：base   源字符串
 *       target 替换目标
 *       source 替换字符串
 * 返回值：符合条件的字符串
 **/
//string Replace(string base, string target, string source);
//
///**
// * 原型：string SubStringWithLine(string base,string pattern)
// * 功能：对搜狐中有|的字符串取值，做特殊处理
// * 参数：base 源字符串
// *       pattern %$$% 任一要取出的字符串
// *               %**% 任一要忽略的字符串
// *               "&*" 两个标记中间的内容是要和结果一起返回的
// * 返回值：符合条件的字符串
// **/
//string SubStringWithLine(string base2, string pattern2);
//
///**
// * 原型：string GetDubleStar(string base,string DubleStar,bool &isLeft)
// * 功能：获取两个&*之间的字符串
// * 参数：base 源字符串
// *       DubleStar &*
// *       isLeft 获取的字符串是在base的左边还是右边
// * 返回值：符合条件的字符串
// **/
//string GetDubleStar(string base2, string DubleStar2, bool &isLeft);
//
///**
// * 原型：string MultiSubstring(string base, string startWith,string pattern)
// * 功能：从startWith处开始查找符合pattern的字符串
// * 参数：base 源字符串
// *        startWith 从此处开始查找
// *        pattern %$$% 任一要取出的字符串
// *                %**% 任一要忽略的字符串
// *                "&*" 两个标记中间的内容是要和结果一起返回的
// * 返回值：符合条件的字符串，多个用分隔符隔开
// **/
//string MultiSubstring(string base2, string startWith2, string pattern2);
//
///**
// * 原型：string SubStringForMulDollar(string base,string pattern)
// * 功能：字符串含有多个%$$%时的处理函数
// * 参数：
// * 返回值：符合条件的字符串
// **/
//string SubStringForMulDollar(string base2, string pattern2);
//
///**
// * 原型：int GetStrCount(string base,string parm)
// * 功能：统计base中parm的个数
// * 参数：
// * 返回值：个数
// **/
//int GetStrCount(string base, string parm);
//
///**
// * 原型：string SetRegxString(string base,string pattern)
// * 功能：字符串含有正则表达式时的处理函数
// * 参数：
// * 返回值：符合条件的字符串
// **/
//string SetRegxString(string base, string pattern);
//
///**
// * 原型：string SubString(string base,string pattern)
// * 功能：字符串含有与pattern时的处理函数
// * 参数：base 源字符串
// *        pattern %$$% 任一要取出的字符串
// *                %**% 任一要忽略的字符串
// *                "&*" 两个标记中间的内容是要和结果一起返回的
// * 返回值：符合条件的字符串
// **/
//string SubString(string base, string pattern);
//
///**
// * 原型：string RequestHttp(LPCSTR urls)
// * 功能：通过curl_lib访问urls
// * 参数：urls :多个地址间‘\n’间隔
// * 返回值：页面源码
// **/
//string RequestHttp(LPCSTR urls);
//string RequestHttpForIPad(LPCSTR urls);
//string HttpGet(LPCSTR urls,LPCSTR ua);
//
///**
// * 原型：int GetDocumentByUrls(LPCSTR url)
// * 功能：通过curl_lib访问urls 的主方法
// * 参数：url :访问地址
// * 返回值：1 成功；0 失败
// **/
//int GetDocumentByUrls(LPCSTR Url,LPCSTR ua);
//
///**
// * 原型：size_t write_callback( void *ptr, size_t size,size_t nmemb, void *stream)
// * 功能：回调函数，接收返回的源码
// *
// * 返回值：页面源码
// **/
//size_t write_callback(void *ptr, size_t size, size_t nmemb, void *stream);
//
///**
// * 原型：void caller(string errorMsg,int level)
// * 功能：调用回调函数
// * 参数：errorMsg 错误信息
// *       level    当前信息的级别
// * 返回值：RT_CODE
// **/
//void caller(string errorMsg, int level);
//
///**
// * 原型：void SplitStr(char res[100][1000],string base,string sep)
// * 功能：分割字符串，将结果存入res中
// * 参数：res :分割结果；base：多个地址间用分隔符间隔；sep：分隔符
// * 返回值：
// **/
//void SplitStr(char res[SPLIT_RES_X][SPLIT_RES_Y], string base, string sep);
//
///**
// * 原型：void Delete_Multi_Array()
// * 功能：集中销毁全局变量中的二维数组
// **/
//void Delete_Multi_Array();
//
///**
// * 原型：Delete_AnalysisRule()
// * 功能：销毁用于存放结果的全局变量
// **/
//void Delete_AnalysisRule();
//
//////////////////////////////////////////////////////////////////////////

/**
 * Morefun Android解析规则
 * 优先返回单段可播放的视频地址
 **/
RT_CODE DLL_EXPORT AnalysisRealUrlForAndroidMorefun(const int sourceId,const LPCSTR url,
													char* realURL, int maxlen, const LPCSTR ua,const LPCSTR sourceKind,
													const LPCSTR spiderName,const LPCSTR videoParam);

//////////////////////////////////////////////////////////////////////////

/**
 * Morefun解析规则
 **/
//RT_CODE DLL_EXPORT AnalysisRealUrlForMorefun(const int sourceId,const LPCSTR url,char* realURL, int len, const LPCSTR ua,const LPCSTR sourceKind);
//
//RT_CODE DLL_EXPORT AnalysisRealUrlForMorefun2(const int sourceId,const LPCSTR url, 
//											  char* realURL, int len, const LPCSTR ua,const LPCSTR sourceKind,
//											  const LPCSTR spiderName,const LPCSTR videoParam);

/**
 * 原型：RT_CODE AnalysisGetRealUrl(const LPCSTR url, char& realURL,int len,size_t timeOutMs);
 * 功能：转换地址，可以设置超时。当转换后的地址是多个时，使用分隔符隔开。
 * 参数：url:要转换的URL
 *       realURL:存放转换后得到的真实可播放的URL的buffer
 *       len:第二个参数分配的长度，如果超出这个大小，则截断超出的部分，只返回前len个字符
 *       timeOut:超时时间，单位：秒，0表示永久等待，默认值为0
 * 返回值：RT_CODE
 **/
RT_CODE DLL_EXPORT AnalysisRealUrl(const int sourceId, const LPCSTR url,char* realURL, int len, size_t timeOut, const LPCSTR sourceKind);

RT_CODE DLL_EXPORT AnalysisRealUrl2(const int sourceId, const LPCSTR url,
									char* realURL, int len, size_t timeOut, const LPCSTR sourceKind,
									const LPCSTR spiderName,const LPCSTR videoParam,int isAndroid=0);

/**
 * 原型：RT_CODE AnalysisGetRealUrl(const LPCSTR url, char& realURL,int len,size_t timeOutMs);
 * 功能：获取详细播放地址
 * 参数：url:要转换的URL
 *		dflag: nd,hd,sd
 *       realURL:存放转换后得到的真实可播放的URL的buffer
 *       len:第二个参数分配的长度，如果超出这个大小，则截断超出的部分，只返回前len个字符
 *       timeOut:超时时间，单位：秒，0表示永久等待，默认值为0
 * 返回值：RT_CODE
 **/
RT_CODE DLL_EXPORT AnalysisRealUrlItems(const int sourceId, const LPCSTR url,char* realURL, int len, size_t timeOut, const LPCSTR dflag);

RT_CODE DLL_EXPORT AnalysisRealUrlItems2(const int sourceId, const LPCSTR url,
										char* realURL, int len, size_t timeOut, const LPCSTR dflag,
										const LPCSTR spiderName,const LPCSTR videoParam);

/**
 * 原型：RT_CODE DLL_EXPORT AnalysisRealUrlFromFlvcd(const LPCSTR url, char* realURL,int len,LPCSTR split, size_t timeOut,LPCSTR format)
 * 功能：通过flvcd.com解析真实播放地址。
 * 参数：url:要转换的URL
 *       realURL:存放转换后得到的真实可播放的URL的buffer
 *       len:第二个参数分配的长度，如果超出这个大小，则截断超出的部分，只返回前len个字符
 *       split:分隔符,如果为NULL，使用默认分隔符
 *       timeOut:超时时间，单位：秒，0表示永久等待，默认值为0
 *       format:模式选择：nd,标清 hd,高清 sd,超清
 * 返回值：RT_CODE
 **/
RT_CODE DLL_EXPORT AnalysisRealUrlFromFlvcd(const int sourceId,const LPCSTR url, char* realURL, int len, size_t timeOut,LPCSTR format);

RT_CODE DLL_EXPORT AnalysisRealUrlFromFlvcd2(const int sourceId,const LPCSTR url, char* realURL,int maxlen,size_t timeOut,LPCSTR format,LPCSTR spiderName);

/**
 * 原型：RT_CODE AnalysisRealUrlFromVerycd(const int sourceId,const LPCSTR url, char* realURL,int maxlen,size_t timeOut,LPCSTR format,LPCSTR spiderName);
 * 功能：解析SWF播放器所需参数。
 * 参数：url:播放页URL
 *       resultStr:存放返回结果buffer
 *       len:第二个参数分配的长度，如果超出这个大小，则截断超出的部分，只返回前len个字符
 *       split:分隔符,如果为NULL，使用默认分隔符
 *       timeOut:超时时间，单位：秒，0表示永久等待，默认值为0
 *		 sourceKind:资源类别
 * 返回值：RT_CODE
 **/
RT_CODE DLL_EXPORT AnalysisRealUrlFromVerycd(const int sourceId,const LPCSTR url, char* realURL,int maxlen,size_t timeOut,LPCSTR format,LPCSTR spiderName);
/**
 * 原型：RT_CODE AnalysisParam(const LPCSTR url, char& realURL,int len,LPCSTR split, size_t timeOutS);
 * 功能：解析SWF播放器所需参数。
 * 参数：url:播放页URL
 *       resultStr:存放返回结果buffer
 *       len:第二个参数分配的长度，如果超出这个大小，则截断超出的部分，只返回前len个字符
 *       split:分隔符,如果为NULL，使用默认分隔符
 *       timeOut:超时时间，单位：秒，0表示永久等待，默认值为0
 *		 sourceKind:资源类别
 * 返回值：RT_CODE
 **/
RT_CODE DLL_EXPORT AnalysisParam(const int sourceId,const LPCSTR url, char* resultStr ,int maxlen, size_t timeOut,const LPCSTR sourceKind);

RT_CODE DLL_EXPORT AnalysisParam2(const int sourceId,const LPCSTR url, char* resultStr ,int maxlen, size_t timeOut,const LPCSTR sourceKind,const LPCSTR spiderName,const LPCSTR videoParam);

/**
 * 原型：RT_CODE AnalysisRealUrlForTV(const int sourceId,string url,char* realURL, int maxlen,const LPCSTR ua,const LPCSTR sourceKind)
 * 功能：将请求地址以JSON格式返回。
 * 参数：url:要转换的URL
 *       realURL:存放转换后得到的真实可播放的URL的buffer
 *       len:第二个参数分配的长度，如果超出这个大小，则截断超出的部分，只返回前len个字符
 *       split:分隔符,如果为NULL，使用默认分隔符
 *       timeOut:超时时间，单位：秒，0表示永久等待，默认值为0
 * 返回值：RT_CODE
 **/
RT_CODE AnalysisRealUrlForTV(const int sourceId, string url, char* realURL,int maxlen, const LPCSTR ua, const LPCSTR sourceKind,const LPCSTR spiderName,const LPCSTR videoParam);

/**
 * 原型：RT_CODE AnalysisSeekOption(const LPCSTR listType, const int seektime, const LPCSTR url, const LPCSTR webUrl, char* seekOption ,int maxlen,const LPCSTR ua);
 * 功能：解析网络视频跳转地址参数。
 * 参数：listType:资源名称
 *       seektime:起始地址
 *       url:播放地址
 *       webUrl:影片地址
 *		 ua: user_agent
 *       seekOption:跳转参数(格式：跳转参数URL串:开始时间)
 *		 maxlen: 长度限制
 * 返回值：RT_CODE
 **/
RT_CODE DLL_EXPORT AnalysisSeekOption(const LPCSTR listType,const int seektime, const LPCSTR url, const LPCSTR webUrl,const LPCSTR ua, char* seekOption, int maxlen);

/**
 * 原型：LPCSTR isReadDemuxPackageForDuration(const LPCSTR listType)
 * 功能：是否需要读取分离器数据
 * 参数：listType
 * 返回值：bool
 **/
bool DLL_EXPORT isReadDemuxPackageForDuration(const LPCSTR listType);

/**
 * 原型：__int64 GetTotalTimeInMsec(const LPCSTR listType, const __int64 totalTime,const int smartTime)
 * 功能：获取播放跳转后的总时长
 * 参数：listType: 资源类型
 *		 startTime: 开始时间
 *		 totalTime: 总时长
 * 返回值：ms
 **/
__int64 DLL_EXPORT GetTotalTimeInMsec(const LPCSTR listType,const __int64 startTime, const __int64 totalTime);

/**
 * 原型：int DLL_EXPORT GetDemuxOpenSeekType(const int sourceId,const LPCSTR playPath)
 * 功能：获取打开视频流方式
 * 返回值：1,默认值 2,m1905\yinyuetai
 **/
int DLL_EXPORT GetDemuxOpenSeekType(const int sourceId, const LPCSTR playPath);

/**
 * 原型：void SetUserAgent(LPCSTR ua)
 * 功能：设置用于下载土豆的UA
 * 参数：LPCSTR UA
 * 返回值：RT_CODE
 **/
RT_CODE DLL_EXPORT SetUserAgent(LPCSTR ua);

/**
 * 原型：const char*  URLTGetVersion()
 * 功能：返回版本号，基本规则：末位为单数表示测试版，末位为双数表示正式版，原则上不要大于128字节
 * 参数：
 * 返回值：版本号
 **/
LPCSTR DLL_EXPORT URLTGetVersion();

/**
 * 原型：const char*  URLTGetDefaultSplit()
 * 功能：获取默认分隔符
 * 参数：无
 * 返回值：默认分隔符
 **/
LPCSTR DLL_EXPORT URLTGetDefaultSplit();

/**
 * 原型：const char* OutputErrorMsg();
 * 功能：输出错误信息
 * 参数：
 * 返回值：错误信息
 **/
LPCSTR DLL_EXPORT OutputErrorMsg();

/**
 * 原型：RT_CODE URLTSetLogLevel(int lv)
 * 功能：设置日志级别
 * 参数：lv 日志级别，默认为 3
 * 返回值：RT_CODE
 **/
RT_CODE DLL_EXPORT URLTSetLogLevel(int lv);

/**
 * 原型：RT_CODE RegistCallBackFun(CallBackFun pf)
 * 功能：注册回调函数
 * 参数：pf 函数指针
 * 返回值：RT_CODE
 **/
RT_CODE DLL_EXPORT RegistCallBackFun(CallBackFun pf);

/**
 * 原型：void URLTDeleteGlobalVar()
 * 功能：销毁全局变量
 **/
void DLL_EXPORT URLTDeleteGlobalVar();

/**
 * 原型：bool URLTIsRunning()
 * 功能：获取当前运行状态
 **/
//bool DLL_EXPORT URLTIsRunning();

/**
 * 获取默认超时时间
 **/
size_t DLL_EXPORT GetRequestTimeOut(const int sourceId,const LPCSTR url, const LPCSTR sourceKind);

size_t DLL_EXPORT GetRequestTimeOut2(const int sourceId,const LPCSTR url, const LPCSTR sourceKind,const LPCSTR spiderName,const LPCSTR videoParam);

/**
 * 原型：RT_CODE DLL_EXPORT SetMfversion(const int mfversion)
 * 功能：设置当前Morefun版本标识 1,MF老版 2,MF新版
 * 参数：int mfversion
 * 返回值：RT_CODE
 **/
RT_CODE DLL_EXPORT SetMfversion(const int mfversion);

/**
 * 原型：RT_CODE DLL_EXPORT SetMfversionCode(const LPCSTR mfversion)
 * 功能：设置当前Morefun版本号
 * 参数：LPCSTR mfversion
 * 返回值：RT_CODE
 **/
RT_CODE DLL_EXPORT SetMfversionCode(const LPCSTR mfversion);

/**
 * 原型：RT_CODE DLL_EXPORT SetClientId(const LPCSTR clid)
 * 功能：设置当前客户端ID
 * 参数：LPCSTR clid
 * 返回值：RT_CODE
 **/
RT_CODE DLL_EXPORT SetClientId(const LPCSTR clid);
//RT_CODE DLL_EXPORT SetClientId(const LPCSTR clid){
//	clientId=clid;
//	return AV_SUCCESS;
//}

/**
 * 原型：LPCSTR DLL_EXPORT GetUserAgent(const LPCSTR spiderName,const LPCSTR url)
 * 功能：获取UA
 * 参数：LPCSTR spiderName
 * 参数：LPCSTR url
 * 返回值：LPCSTR
 **/
LPCSTR DLL_EXPORT GetUserAgent(const LPCSTR spiderName,const LPCSTR url);

/**
 * 原型：bool DLL_EXPORT IsSupportWebAnalysis(const LPCSTR spiderName,const LPCSTR browserName,const LPCSTR url);
 * 功能：是否支持Web解析
 * 参数：LPCSTR spiderName
 * 参数：LPCSTR browserName
 * 参数：LPCSTR url
 * 返回值：bool
 **/
bool DLL_EXPORT IsSupportWebAnalysis(const LPCSTR spiderName,const LPCSTR browserName,const LPCSTR url);

bool DLL_EXPORT IsSupportAnalysis(const LPCSTR spiderName,const LPCSTR url);

//////////////////////////////////////////////////////////////////////////
#ifdef __cplusplus
}

#endif

#endif // __MAIN_H__
