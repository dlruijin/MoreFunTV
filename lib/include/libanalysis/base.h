/*
 *
 */
#ifndef __BASE_H__
#define __BASE_H__
#define _CRTDBG_MAP_ALLOC 

#define __MAC__

#include <stdlib.h>
// wdy added ----> 20120528 for Linux
#ifdef _WINDOWS
#include <crtdbg.h>
#else
//LPSTR)(LPCTSTR
typedef const wchar_t* LPCTSTR;
typedef const char* LPCSTR;
typedef const char* LPCSTR;
typedef char* LPSTR;
//typedef bool BOOL;
typedef long long __int64;

#define CALLBACK
#define DLL_EXPORT
#endif
// wdy added <----
//#include <exception>
//#include <vector>
//#include <iostream>
//#include <fstream>
//#include <stdio.h>

#include "boost/algorithm/string.hpp"
#include "boost/regex.hpp"
#include "boost/lexical_cast.hpp"
#include "boost/multi_array.hpp"
#include "curl/curl.h"
//#include "tinyxml.h"

using namespace std;
using namespace boost;

const string _version = "2.14.20140514"; //DLL版本号

////////////////////////////////////////////////////////////////////////////
////返回值
//typedef enum _RT_CODE {
//	AV_SUCCESS = 0, // 成功
//	AV_UNKOWN = -1, // 未知错误
//	AV_TIMEOUT = -2, // 超时
//	AV_PARAMS_ERROR = -3, // 参数错误
//	AV_PARSE_ERROR = -4, //地址无效
//	AV_STRING_CATCH = -5, //字符串处理异常
//	AV_STRING_ERROR = -6, //字符串处理错误
//	AV_NO_MATCH_ADD = -7, //未找到解析规则
//	AV_RULE_INIT_ERROR = -8, //规则加载失败
//	AV_VERSION_ERROR = -9, //对应的规则版本不匹配
//	AV_VERSION_MALFORMAT = -10, //版本号格式不正确
//	AV_REGISTFUN_ERROR = -11, //注册回调函数失败
//	AV_SETLOGLEVEL_ERROR = -12, //设置日志级别失败
//	AV_SET_UA_ERROR = -13,//设置UA失败
//} RT_CODE;

// LOG 级别
#define LOGDEBUG   0
#define LOGINFO    1
#define LOGNOTICE  2
#define LOGWARNING 3
#define LOGERROR   4
#define LOGSEVERE  5
#define LOGFATAL   6
#define LOGNONE    7

#define VIDEO_FORMAT_M3U8 "m3u8"
#define ANDROID_CLIENT_ID "400"

//////////////////////////////////////////////////////////////////////////
//数组大小定义
const int ARRAY_COUNT = 2; //定义数组数
const int ARRAY_COUNT_X = 20; //定义数组数
const int ARRAY_COUNT_Y = 80; //定义数组数
const int PARM_MAX_COUNT = 10; //参数数组最大数
const int SPLIT_RES_X = 80; //分割字符串结果集
const int SPLIT_RES_Y = 2500; //分割字符串结果集
const int WHILE_MAX = 200; //循环最大次数，超过则认为出错
const int XML_MAX_COUNT = 400; //xml内容最大行数

//用于处理字符串的分割符
const string REGX1 = "{^";
const string REGX2 = "^}";
const string DOLLAR = "%$$%";
const string STAR = "%**%";
const string SLASH = "/";
const string DubleStar = "&*";
const string Plus = "$+";
const string ParmSplitLeft = "<%";
const string ParmSplitRight = "%>";

const string FLVCD_PARSE_URL = "http://www.flvcd.com/parse.php"; //flvcd.com
const string VERYCD_PARSE_URL = "http://www.verycd.com/api/v2/base/playlink/get_playinfo?url="; //verycd.com
const int MIX_STRING_COUNT = 69; //优酷网站生成SID用到的字符集
const char BASE_MIX_STRING[MIX_STRING_COUNT] ="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/\\:._-1234567890";

//////////////////////////////////////////////////////////////////////////
extern LPCSTR DefaultSplit; //默认分隔符
extern size_t DefaultTimeOut; //默认超时时间，0代表无限制
extern int _sourceId;
extern string _url ;
extern string _UA;//请求User-Agent
extern string _DFUA;//默认User-Agent
extern string _UA_IPAD; //iPad
extern string _REFERER; //Header参数
extern string _POSTDATA ; //提交数据
extern string _RESCHARSET; //字符编码
extern string _qRes; //存放RequestHttp方法的结果
extern int _logLevel; //日志级别
extern bool isRunning; //运行状态

typedef boost::multi_array<string, ARRAY_COUNT> array2_type; //定义模板
extern array2_type _AnalysisRule; //定义用于存放执行结果的数组
extern array2_type array2_null; //定义二维空数组

//定义函数指针
typedef void (CALLBACK* CallBackFun)(LPCSTR errorMsg);
extern CallBackFun pff;

extern string _errorMsg; //定义全局错误信息
extern RT_CODE _errorCode; //定义全局错误码
extern string _XmlVersion; //xml文件的版本号
extern int mfversion; //morefun版本标识
extern string mfversion_code; //morefun版本字符
extern string clientId; //客户端ID
extern string WEB_APPS[30];
extern string WEB_APPS_ANDROID[30];
extern string ANALYSIS_SUPPORT[30];
extern string WEBPLAY_SUPPORT_ANDROID[30];

// wdy added ----> 20120528 for Linux
#ifdef _WINDOWS
#define BUILD_DLL
#ifdef BUILD_DLL
#define DLL_EXPORT __declspec(dllexport)
#else
#define DLL_EXPORT __declspec(dllimport)
#endif
#endif
// wdy added <----


// wdy added ----> 20120528 for Linux
#ifndef _WINDOWS
void itoa(unsigned long val, char *buf, unsigned radix);
#endif
// wdy added <----

#endif // __BASE_H__
