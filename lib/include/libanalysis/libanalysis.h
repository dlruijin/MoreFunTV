//
//  libanalysis.h
//  libanalysis
//
//  Created by WuDongyang on 14-10-24.
//  Copyright (c) 2014年 WuDongyang. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AnalysisHelper.h"

@interface libanalysis : NSObject


//////////////////////////////////////////////////////////////////////////

//DEFINE_METHOD1(RT_CODE, RegistCallBackFun, (void (CALLBACK* p1)(LPCSTR errorMsg)) )


/**
 * 原型：RT_CODE RegistCallBackFun(CallBackFun pf)
 * 功能：注册回调函数
 * 参数：pf 函数指针
 * 返回值：RT_CODE
 **/
- (RT_CODE) RegistCallBackFun:(CallBackFun) pf;

/**
 * 原型：RT_CODE URLTSetLogLevel(int lv)
 * 功能：设置日志级别
 * 参数：lv 日志级别，默认为 3
 * 返回值：RT_CODE
 **/
- (RT_CODE) URLTSetLogLevel:(int) lv;

/**
 * 原型：void SetUserAgent(LPCSTR ua)
 * 功能：设置用于下载土豆的UA
 * 参数：LPCSTR UA
 * 返回值：RT_CODE
 **/
- (RT_CODE) SetUserAgent:(LPCSTR)ua;


/**
 * 原型：LPCSTR DLL_EXPORT GetUserAgent(const LPCSTR spiderName,const LPCSTR url)
 * 功能：获取UA
 * 参数：LPCSTR spiderName
 * 参数：LPCSTR url
 * 返回值：LPCSTR
 **/
// LPCSTR DLL_EXPORT GetUserAgent(const LPCSTR spiderName,const LPCSTR url);
- (LPCSTR) GetUserAgent:(const LPCSTR)spiderName url:(const LPCSTR) url;

/**
 * 原型：bool URLTIsRunning()
 * 功能：获取当前运行状态
 **/
- (bool) URLTIsRunning;


/**
 * Morefun解析规则
 **/
//RT_CODE DLL_EXPORT AnalysisRealUrlForMorefun(const int sourceId,const LPCSTR url,char* realURL, int len, const LPCSTR ua,const LPCSTR sourceKind);
//
//RT_CODE DLL_EXPORT AnalysisRealUrlForMorefun2(const int sourceId,const LPCSTR url,
//                                              char* realURL, int len, const LPCSTR ua,const LPCSTR sourceKind,
//                                              const LPCSTR spiderName,const LPCSTR videoParam);
- (RT_CODE) AnalysisRealUrlForMorefun: (const int)sourceId url:(const LPCSTR)url realurl:(char *)realURL len:(int)len ua:(const LPCSTR)ua skind:(const LPCSTR) sourceKind;


- (RT_CODE) AnalysisRealUrlForMorefun2:(const int)sourceId url:(const LPCSTR)url realurl:(char *)realURL len:(int)len ua:(const LPCSTR)ua skind:(const LPCSTR) sourceKind spiderName:(const LPCSTR)spiderName videoParam:(const LPCSTR)videoParam ;

- (RT_CODE) AnalysisRealUrlForAndroidMorefun:(const int)sourceId url:(const LPCSTR)url realurl:(char *)realURL len:(int)len ua:(const LPCSTR)ua skind:(const LPCSTR) sourceKind spiderName:(const LPCSTR)spiderName videoParam:(const LPCSTR)videoParam ;


/**
 * 原型：RT_CODE AnalysisGetRealUrl(const LPCSTR url, char& realURL,int len,size_t timeOutMs);
 * 功能：获取详细播放地址
 * 参数：url:要转换的URL
 *		dflag: nd,hd,sd
 *       realURL:存放转换后得到的真实可播放的URL的buffer
 *       len:第二个参数分配的长度，如果超出这个大小，则截断超出的部分，只返回前len个字符
 *       timeOut:超时时间，单位：秒，0表示永久等待，默认值为0
 * 返回值：RT_CODE
 **/
- (RT_CODE) AnalysisRealUrlItems: (const int)sourceId url:(const LPCSTR)url realURL:(char*)realURL len:(int)len timeOut:(size_t)timeOut dflag:(const LPCSTR)dflag;

- (RT_CODE) AnalysisRealUrlItems2: (const int)sourceId url:(const LPCSTR)url realURL:(char*)realURL len:(int)len timeOut:(size_t)timeOut dflag:(const LPCSTR)dflag spiderName:(const LPCSTR)spiderName videoParam:(const LPCSTR)videoParam;




@end
