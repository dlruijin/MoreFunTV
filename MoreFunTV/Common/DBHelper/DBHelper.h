//
//  DBHelper.h
//  MoreFunTV
//
//  Created by admin on 14-11-7.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"

@interface DBHelper : NSObject
//打开数据库
+(FMDatabase *)openDatabase;
@end
