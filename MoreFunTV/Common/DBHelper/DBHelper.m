//
//  DBHelper.m
//  MoreFunTV
//
//  Created by admin on 14-11-7.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "DBHelper.h"

@implementation DBHelper
//打开数据库
+(FMDatabase *)openDatabase
{
    //paths： ios下Document路径，Document为ios中可读写的文件夹
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
//    NSLog(@"%@",documentDirectory);
//    dbPath： 数据库路径，在Document中。
    NSString *dbPath = [documentDirectory stringByAppendingPathComponent:@"MoreFunTV1.db"];
    
    //创建数据库实例 db  这里说明下:如果路径中不存在"Test.db"的文件,sqlite会自动创建"Test.db"
    FMDatabase *db= [FMDatabase databaseWithPath:dbPath] ;
    if (![db open])
    {
        return nil;
    }
    else
    {
        return db;
    }
}
@end
