//
//  Global.h
//  MoreFunTV
//
//  Created by admin on 14-10-21.
//  Copyright (c) 2014Âπ? admin. All rights reserved.
//

#import <Foundation/Foundation.h>

//多语言
#define MyLocalizedString(key) \
[[JAInternationalControl bundle] localizedStringForKey:(key) value:nil table:@"InfoPlist"]

//获取屏幕高度
#define kScreenHeight [UIScreen mainScreen].bounds.size.height
#define kScreenWidth [UIScreen mainScreen].bounds.size.width

//分发服务器接口
#define clientURL @"http://dispatch.morefun.tv/getNewServerIp.php?"

//客户端名称
#define clientName @"&clientname=morefuntvios2014"

//客户端ID iOS为500
#define clientID @"&clientid=500"

#define cID @"500"

//渠道名
#define pchannel @"&pchannel=morefuntv"

//clientversion  版本号 使用应用本身版本号，但注意需大于0.8.0.0
#define clientVersion @"&clientversion=1.1.1"

//网络接口地址
#define apiURL @"http://api.cube.morefun.tv"
//#define apiURL @"http://121.40.92.3:8080"

//资源数据 直播列表数据 推荐信息列表接口
#define resources @"/cube/api/resources?"

//热词信息查询接口
#define hotWords @"/cube/api/hotwords?"

//历史记录接口
#define historyRecord @"/cube/api/users/989/history_play?"

//详细资源接口
#define detailResource @"/cube/api/resources/%@?"

//直播数据EPG信息
#define EPG @"/cube/api/resources/%@/epgs?"

//资源搜索接口
#define searchRe @"/cube/api/resources/search/%@?"

//联想搜索接口
#define searchLx @"http://tip.soku.com/search_keys?query=%@"

//获取服务器接口
#define clients @"/cube/api/clients/%@?"

//获取一级应用及子应用
#define subapps @"/cube/api/apps/%@/subapps?"


//安全校验参数 用户类型 用户ID 客户端ID 时间戳 加密 活动（请求这个动作）
#define safeCheckParameter @"&user_type=1&user_id=0&client_id=500&session_id=dltianwei&timestamp=%@&security_code=%@"

//页码
#define pageNo @"&page_no=%@"

//数据个数
#define pageSize @"&page_size=%@"

//分类ID
#define categoryID @"&category_id=%@"

//是否有效
#define isActives @"&is_active=%@"

//是否锁定
#define isLocked @"&is_locked=%@"

//appID
#define appID @"&app_id=%@"

//地区ID
#define areaID @"&area_id=%@"

//风格ID
#define styleID @"&style_id=%@"

//起始年份
#define releasedOnFrom @"&released_on_from=%@"

//截止年份
#define releasedOnTo @"&released_on_to=%@"

//数据个数released_on DESC
#define orderBy @"&order_by=%@"


//直播start_on开始时间
#define startOnFrom @"&start_on_from=%@"

//直播start_on结束时间
#define startOnTo @"&start_on_to=%@"

//查询分类信息
#define classURL @"http://files2.morefun.tv/mftypes/%@.json"

//请求方法
#define Method @"&_method=GET"



