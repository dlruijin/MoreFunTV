//
//  ClientXMLViewController.m
//  MoreFunTV
//
//  Created by admin on 14-10-30.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "ClientXMLViewController.h"

@interface ClientXMLViewController ()

@end

@implementation ClientXMLViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

//客户端信息解析
-(NSString *)ClientXMLAnalysis
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@%@%@",clientURL,clientName,clientVersion,clientID,pchannel];
    NSXMLParser *parser = [[NSXMLParser alloc]initWithContentsOfURL:[NSURL URLWithString:strUrl]];
    [parser setDelegate:self];//设置NSXMLParser对象的解析方法代理
    [parser setShouldProcessNamespaces:NO];
    [parser parse];//开始解析
    return _strApiURL;
}

#pragma mark xmlparser
//step 1 :准备解析
- (void)parserDidStartDocument:(NSXMLParser *)parser
{
}
//step 2：准备解析节点
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"appserver"])
    {
	}
    
    if ([elementName isEqualToString:@"appserverinfo"])
    {
	}
}
//step 3:获取首尾节点间内容
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    self.strCurrent = [[NSMutableString alloc]init];
    [self.strCurrent setString:@""];
    [self.strCurrent appendString:string];
}

//step 4 ：解析完当前节点
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    NSString *str = [[NSString alloc] initWithString:self.strCurrent];
    
    if ([elementName isEqualToString:@"servername"])
    {
        //servername为服务器名；
//        NSLog(@"服务器名为: %@",str);
    }
    else if ([elementName isEqualToString:@"serverip"])
    {
        //serverip为访问猫范后台接口的域名；
        _strApiURL = str;
//        NSLog(@"访问猫范后台接口的域名为: %@",_strApiURL);
    }
    else if ([elementName isEqualToString:@"updateserver"])
    {
        //updateserver为客户端升级地址；
//        NSLog(@"客户端升级地址为: %@",str);
    }
    else if ([elementName isEqualToString:@"autoupdate"])
    {
        //autoupdate为真实播放地址解析库升级地址；
//        NSLog(@"真实播放地址解析库升级地址: %@",str);
    }
    else if ([elementName isEqualToString:@"rssfeeds"])
    {
        //rssfeeds为RSS通知数据地址；
//        NSLog(@"RSS通知数据地址: %@",str);
    }
    else if ([elementName isEqualToString:@"feedbackserver"])
    {
        //feedbackserver为日志反馈地址；
//        NSLog(@"日志反馈地址为: %@",str);
    }
    else if ([elementName isEqualToString:@"feedbacklevel"])
    {
        //feedbacklevel为日志反馈log等级；
//        NSLog(@"日志反馈log等级为: %@",str);
    }
}

//step 5；解析结束
- (void)parserDidEndDocument:(NSXMLParser *)parser
{
}

//获取cdata块数据
- (void)parser:(NSXMLParser *)parser foundCDATA:(NSData *)CDATABlock
{
    self.data =[[NSString alloc]initWithData:CDATABlock encoding:NSUTF8StringEncoding];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
