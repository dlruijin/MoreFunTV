//
//  HomeViewController.m
//  MoreFunTV
//
//  Created by admin on 14-10-21.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "HomeViewController.h"
#import "AppDelegate.h"
#import "UIImageView+AFNetworking.h"

@interface HomeViewController (){
    __weak MatchstickDeviceController *_matchstickDeviceController;
}

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //投射控制界面赋值
    AppDelegate *delegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
    _matchstickDeviceController = delegate.matchstickDeviceController;
    //投射图标判断
    if (_matchstickDeviceController.deviceScanner.devices.count > 0) {
        self.tabBarController.navigationItem.rightBarButtonItem = _matchstickDeviceController.matchstickBarButton;
    }
    
    //添加推荐视频信息列表
    [self addTableRecommend];
    
    //加载不到数据时的提示
    [self alertShow];
    
    //获取通知中心  浮动按钮监测的方法
    NSNotificationCenter *notificationMyUIButton = [NSNotificationCenter defaultCenter];
    //添加通知到中心
    [notificationMyUIButton addObserver:self selector:@selector(myUIButton:) name:@"myUIButton" object:nil];
    
    [Loading showLoadingWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{//执行耗时操作
        //获取服务器
        [self requestClient];
        //获取客户端 获取一级应用
        [self requestSubapps];
        dispatch_async(dispatch_get_main_queue(), ^{//主线程更新UI
            [Loading hiddonLoadingWithView:self.view];
            //加载不到数据
            if (_setStrApiURL.length == 0 || _client == nil || _arrSubDatail.count == 0 || _arr.count == 0 || _arrVoide.count == 0 || _arrRec.count == 0) {
                [UIView animateWithDuration:0.2f animations:^{
                    [_viel setHidden:NO];
                }];
                return ;
            }
            [self resetCell];
            [_tableRecommend reloadData];
        });
    });
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     _matchstickDeviceController.delegate = self;
    
    //TiTle自定义
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 60, 44)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:19.0f];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = MyLocalizedString(@"tuijian");
    self.tabBarController.navigationItem.titleView = titleLabel;
}

- (void)shouldDisplayModalDeviceController {
    //投射按钮调用的方法
    [[NSNotificationCenter defaultCenter] postNotificationName:@"myDevice" object:nil userInfo:nil];
}

- (void)didDiscoverDeviceOnNetwork
{
    // Add the Matchstick icon if not present.
    self.tabBarController.navigationItem.rightBarButtonItem = _matchstickDeviceController.matchstickBarButton;
}

//浮动按钮监测的通知事件
-(void)myUIButton:(NSNotification *)notification{
    SetUpSingleton *set = [SetUpSingleton sharedSetUp];
    NSString *titleString = [SetUpSingleton sharedSetUp]._titleString;
    set.detail.strTitle = titleString;
//    NSLog(@"_______________%@",[SetUpSingleton sharedSetUp]._titleString);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateHistoryPlay" object:nil userInfo:nil];
    //进入投射控制页
    MatchStickViewController *matVc = [[MatchStickViewController alloc]initWithRealUrl:set.strTouSheURL];
    matVc.mediaToPlay = set.detail;
    
    matVc.strAllRealUrl = set.strAllTouSheURL;
    [self presentViewController:matVc animated:YES completion:^{
    }];
}

//获取服务器
-(void)requestClient
{
    //单例
    SetUpSingleton *set = [SetUpSingleton sharedSetUp];
    ClientXMLViewController *clientXml = [[ClientXMLViewController alloc]init];
    _setStrApiURL = [clientXml ClientXMLAnalysis];
    
    if (_setStrApiURL.length != 0) {
        set.strApiURL = _setStrApiURL;
        RequestParameter *rep = [[RequestParameter alloc]init];
        rep.strKeyWord = @"clients";
        _client = [NetWorkingHelper getClientInfoWithRep:rep];
        if (_client != nil) {
            set.strRootAppId = _client.strRootAppId;
        }
    }
}

//获取客户端 获取一级应用
-(void)requestSubapps
{
    SetUpSingleton *set = [SetUpSingleton sharedSetUp];
    RequestParameter *rep = [[RequestParameter alloc]init];
    rep.strPageSize = @"500";
    rep.strKeyWord = @"subapps";
    rep.strRootAppId = set.strRootAppId;
    rep.portID = 3;
    rep.strPageNo = @"1";
    rep.strPageSize = @"6";
    
    //请求客户端信息
    NSMutableArray *arrSub = [NetWorkingHelper getSubappsInfoWithRep:rep];
    Subapps *sub = [arrSub objectAtIndex:0];
    set.strSubappId = sub.strSubappId;
    rep.strRootAppId = set.strSubappId;
    _arrSubDatail = [NetWorkingHelper getSubappsDeailWithRep:rep];

    //加载不到数据
    if (_arrSubDatail.count != 0) {
        //请求推荐分类和大图信息
        rep.strKeyWord = @"";
        _arrVoide = [[NSMutableArray alloc]init];
        for (int i = 0; i < _arrSubDatail.count; i++)
        {
            Subapps *subApp = [_arrSubDatail objectAtIndex:i];
            rep.strAppID = subApp.strSubappId;
            if (i == 0) {//大图信息
                _arrRec = [NetWorkingHelper getRecommendInfoWithRequestParameter:rep];
            }
            else{//推荐分类信息
                _arr = [NetWorkingHelper getRecommendInfoWithRequestParameter:rep];
                //加载不到数据
                if (_arr.count != 0){
                    [_arrVoide addObject:_arr];
                }
            }
        }
    }
}

//加载不到数据时的提示
-(void)alertShow
{
    _viel = [[UIView alloc]initWithFrame:CGRectMake(0, 64, kScreenWidth, kScreenHeight-48-64)];
    _viel.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000];
    _viel.clipsToBounds = YES;
    [_viel setUserInteractionEnabled:YES];
    [_viel setHidden:YES];
    [self.view addSubview:_viel];
    
    //提示图片
    UIImageView *imgPrompt = [[UIImageView alloc]initWithFrame:CGRectMake((kScreenWidth-40)/2, 64+(kScreenHeight-48-64)/3-90, 40, 40)];
    [imgPrompt setImage:[UIImage imageNamed:@"dggddhgdhgd.png"]];
    imgPrompt.layer.borderWidth = 1.0;
    imgPrompt.layer.borderColor = [[UIColor clearColor]CGColor];
    imgPrompt.clipsToBounds = YES;
    imgPrompt.layer.cornerRadius = 20.0;
    [_viel addSubview:imgPrompt];
    
    UILabel *labTiShi = [[UILabel alloc]initWithFrame:CGRectMake(0, 64+(kScreenHeight-48-64)/3-50, kScreenWidth, 30)];
    labTiShi.textColor = [UIColor blackColor];
    labTiShi.text = @"未加载到数据";
    labTiShi.font = [UIFont boldSystemFontOfSize:18.0f];
    labTiShi.textAlignment = NSTextAlignmentCenter;
    labTiShi.backgroundColor = [UIColor clearColor];
    [_viel addSubview:labTiShi];
    
    UILabel *labCheck = [[UILabel alloc]initWithFrame:CGRectMake(0, 64+(kScreenHeight-48-64)/3-20, kScreenWidth, 30)];
    labCheck.textColor = [UIColor lightGrayColor];
    labCheck.text = @"请检查网络状况";
    labCheck.font = [UIFont boldSystemFontOfSize:18.0f];
    labCheck.textAlignment = NSTextAlignmentCenter;
    labCheck.backgroundColor = [UIColor clearColor];
    [_viel addSubview:labCheck];
    
    //刷新按钮
    UIButton *btnRefresh = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnRefresh.frame = CGRectMake(kScreenWidth/3, 64+(kScreenHeight-48-64)/3+30, kScreenWidth/3, 40);
    btnRefresh.backgroundColor = [UIColor lightGrayColor];
    btnRefresh.layer.borderWidth = 1.0;
    btnRefresh.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    btnRefresh.layer.cornerRadius = 10.0;
    btnRefresh.clipsToBounds = YES;
    btnRefresh.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    [btnRefresh setTitle:MyLocalizedString(@"RefreshC") forState:UIControlStateNormal];
    [btnRefresh setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnRefresh addTarget:self action:@selector(clickRefresh) forControlEvents:UIControlEventTouchUpInside];
    [_viel addSubview:btnRefresh];
}

//点击刷新的事件
-(void)clickRefresh{
    [UIView animateWithDuration:0.2f animations:^{
        [_viel setHidden:YES];
    }];
    
    [Loading showLoadingWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{//执行耗时操作
        //获取服务器
        [self requestClient];
        
        //获取客户端 获取一级应用
        [self requestSubapps];
        dispatch_async(dispatch_get_main_queue(), ^{//主线程更新UI
            [Loading hiddonLoadingWithView:self.view];
            //加载不到数据
            if (_arrRec.count == 0 || _arrVoide.count == 0) {
                [UIView animateWithDuration:0.2f animations:^{
                    [_viel setHidden:NO];
                }];
            }
            [self resetCell];
            [_tableRecommend reloadData];
        });
    });
}


//添加热门影片显示的的ScrollView
-(void)addScrollView
{
    //_scrollViewHot不为空
    if (_scrollViewHot) {
        return;
    }
    //设置UIScrollView大小
    _scrollViewHot = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 180)];
    //设置滚动区域 如果比坐标设置的宽高小将不能滚动
    _scrollViewHot.contentSize = CGSizeMake(320*(_arrRec.count+2),0);
    _scrollViewHot.contentOffset = CGPointMake(320, 0);
    _scrollViewHot.backgroundColor = [UIColor whiteColor];
    //整页屏滚动
    _scrollViewHot.pagingEnabled = YES;
    _scrollViewHot.delegate = self;
    //设置是否显示滚条
    _scrollViewHot.showsVerticalScrollIndicator = NO;
    _scrollViewHot.showsHorizontalScrollIndicator = NO;
    _scrollViewHot.bounces = NO;
    [_viewTitleBg addSubview:_scrollViewHot];
    
    //将大图图片地址存入数组  5 0 1 2 3 4 5 0的方式放入
    NSMutableArray *arrImg = [[NSMutableArray alloc]init];
    NSString *strOne = @"";
    NSString *strLast = @"";
    for (int i = 0; i < _arrRec.count; i++) {
        VideoInfo *vinfo = [_arrRec objectAtIndex:i];
        if (i == 0) {
            strOne = vinfo.strThumbnail;
        }
        else if (i == _arrRec.count-1)
        {
            strLast = vinfo.strThumbnail;
            [arrImg addObject:strLast];
        }
    }
    
    for (int i = 0; i < _arrRec.count; i++) {
        VideoInfo *vIfo = [_arrRec objectAtIndex:i];
        NSString *strUrl = vIfo.strThumbnail;
        [arrImg addObject:strUrl];
    }
    [arrImg addObject:strOne];
    
    for (int i = 0; i < arrImg.count; i++)
    {
        UIButton *btnTuiJian = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        btnTuiJian.frame = CGRectMake(kScreenWidth*i, 0, kScreenWidth, 180);
        [btnTuiJian setUserInteractionEnabled:YES];
        btnTuiJian.backgroundColor = [UIColor clearColor];
        btnTuiJian.clipsToBounds = YES;
        btnTuiJian.tag = 1011 + i;
        [btnTuiJian addTarget:self action:@selector(clickTuiJian:) forControlEvents:UIControlEventTouchUpInside];
        [_scrollViewHot addSubview:btnTuiJian];
        
        UIImageView *img = [[UIImageView alloc]init];
        [img setFrame:CGRectMake(0, 0, kScreenWidth, 180)];
        [img setImageWithURL:[NSURL URLWithString:[arrImg objectAtIndex:i]] placeholderImage:[UIImage imageNamed:@"tuijiandatu.png"]];
        [btnTuiJian addSubview:img];
    }
    
    _labTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 180-30, kScreenWidth-80, 30)];
    _labTitle.backgroundColor = [UIColor colorWithWhite:0.216 alpha:0.900];
    _labTitle.font = [UIFont systemFontOfSize:16.0f];
    _labTitle.textColor = [UIColor whiteColor];
    _labTitle.textAlignment = NSTextAlignmentLeft;
    VideoInfo *vi = [_arrRec objectAtIndex:0];
    _labTitle.text = [NSString stringWithFormat:@"   %@",vi.strTitle];
    [_viewTitleBg addSubview:_labTitle];
    
    //定义PageControll
    _pageControl = [[UIPageControl alloc]init];
    _pageControl.backgroundColor = [UIColor colorWithWhite:0.216 alpha:0.900];
    //指定位置大小
    _pageControl.frame = CGRectMake(kScreenWidth-80, 180-30, 80, 30);
    //指定页面个数  白点个数
    _pageControl.numberOfPages = _arrRec.count;
    _pageControl.clipsToBounds = YES;
    [_pageControl setUserInteractionEnabled:YES];
    //指定未选中的点的颜色
    _pageControl.pageIndicatorTintColor = [UIColor colorWithRed:192.0/255.0 green:192.0/255.0 blue:192.0/255.0 alpha:1.000];
    //指定选中的点的颜色
    _pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:0.197 green:0.571 blue:0.846 alpha:1.000];
    //指定pageControll的值，默认选中的小白点（第一个）
    _pageControl.currentPage = 0;
    [_pageControl addTarget:self action:@selector(changePage)forControlEvents:UIControlEventValueChanged];
    [_viewTitleBg addSubview:_pageControl];
    
    [self performSelector:@selector(switchFocusImageItems) withObject:nil afterDelay:3.0f];
}
//自动变换图片
- (void)switchFocusImageItems
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(switchFocusImageItems) object:nil];
    
    
//    CGFloat pageWidth = _scrollViewHot.frame.size.width;
    int page = floor((_scrollViewHot.contentOffset.x - 320 / 2) / 320) + 1;
    int currentPageIndex=page;
    
    if (currentPageIndex==([_arrRec count])) {
        
        [_scrollViewHot setContentOffset:CGPointMake(0, 0)];
        currentPageIndex = 0;
    }
    
    VideoInfo *vinfo = [_arrRec objectAtIndex:currentPageIndex];
    _labTitle.text = [NSString stringWithFormat:@"   %@",vinfo.strTitle];
    
    CGFloat targetX = _scrollViewHot.contentOffset.x + _scrollViewHot.frame.size.width;
    
    [_scrollViewHot setContentOffset:CGPointMake(targetX, 0) animated:YES] ;
    
    [self performSelector:@selector(switchFocusImageItems) withObject:nil afterDelay:3.0f];
}

-(void)resetCell
{
    //释放cell
    if (_arrCell) {
        _arrCell = nil;
    }
    _arrCell = [[NSMutableDictionary alloc] init];
}

//添加推荐视频信息列表
-(void)addTableRecommend
{
    [self resetCell];
    UIView *aView = [[UIView alloc]init];
    [self.view addSubview:aView];
    //推荐视频信息列表
    _tableRecommend = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, kScreenWidth, kScreenHeight-64-48) style:UITableViewStyleGrouped];
    //隐藏纵向滚动条
    _tableRecommend.showsVerticalScrollIndicator = NO;
    _tableRecommend.delegate = self;
    _tableRecommend.dataSource = self;
    _tableRecommend.bounces = NO;
    //隐藏分割线
    [_tableRecommend setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [_tableRecommend setBackgroundColor:[UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000]];
    [self.view addSubview:_tableRecommend];
}

//推荐视频列表的点击方法
-(void)clickTuiJian:(UIButton *)sender
{
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    VideoInfo *vide = [[VideoInfo alloc]init];
    [arr addObject:vide];
    for (int i = 0; i < _arrRec.count; i++) {
        VideoInfo *vin = [_arrRec objectAtIndex:i];
        [arr addObject:vin];
    }
    [arr addObject:vide];
    
    VideoInfo *vinfo = [arr objectAtIndex:sender.tag-1011];
    DetailViewController *detail = [[DetailViewController alloc]initWithResId:vinfo.strVideoId];
    detail.strTitle = vinfo.strTitle;
    detail.strIndex = @"1";
    detail.strSource = @"";
    [self.navigationController pushViewController:detail animated:YES];
}


//scrollView滚动时调用
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == _scrollViewHot)
    {
        //通过滚动的偏移量来判断目前页面所对应的小白点  x为横坐标
        int page = scrollView.contentOffset.x / 320;
        //_pagecontroll响应值的变化
        page --;
        if (page >= _pageControl.numberOfPages)
        {
            page = 0;
        }
        else if (page < 0)
        {
            page = _pageControl.numberOfPages -1;
        }
        
        _pageControl.currentPage = page;
    }
}

//pagecontroll的委托方法
-(void)changePage
{
    //根据pagecontroll的值来改变_scrollView的滚动位置，以此切换到指定的页面
    [_scrollViewHot setContentOffset:CGPointMake(320 * _pageControl.currentPage, 0)];
}

//偏移  控制scrollView循环滑动
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //tableview和scrollView都有的方法
    if (scrollView == _scrollViewHot)
    {
        VideoInfo *vinfo;
        int page = scrollView.contentOffset.x / 320;
        if (page == 0)
        {
            vinfo = [_arrRec objectAtIndex:_arrRec.count-1];
            [scrollView setContentOffset:CGPointMake(320*_arrRec.count, 0) animated:NO];
        }
        else if (page == _arrRec.count+1)
        {
            [scrollView setContentOffset:CGPointMake(320, 0) animated:NO];
            vinfo = [_arrRec objectAtIndex:0];
        }
        else
        {
            vinfo = [_arrRec objectAtIndex:page-1];
        }
        _labTitle.text = [NSString stringWithFormat:@"   %@",vinfo.strTitle];
    }
}

//设置每个分区多少行
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

//返回分组的个数
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _arrVoide.count;
}

//分组自定义
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        //标题上的背景图片为空
        if (!_viewTitleBg) {
            _viewTitleBg = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 220)];
        }
        _viewTitleBg.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000];
        
        //添加热门影片显示的的ScrollView
        [self addScrollView];
        
        //分类按钮
        UIButton *btnClass = [[UIButton alloc]initWithFrame:CGRectMake(0, 180, kScreenWidth, 40)];
        [btnClass setUserInteractionEnabled:YES];
        [btnClass setBackgroundColor:[UIColor clearColor]];
        btnClass.tag = 210;
        [btnClass addTarget:self action:@selector(clickClass:) forControlEvents:UIControlEventTouchUpInside];
        [_viewTitleBg addSubview:btnClass];
        
        UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 40)];
        lab.backgroundColor = [UIColor clearColor];
        lab.font = [UIFont boldSystemFontOfSize:19.0f];
        [lab setUserInteractionEnabled:NO];
        [btnClass addSubview:lab];
        
        UIView *viewLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.5)];
        viewLine.backgroundColor = [UIColor colorWithWhite:0.656 alpha:1.000];
        [lab addSubview:viewLine];
        
        //蓝色标识
        UIImageView *imgArrow = [[UIImageView alloc]initWithFrame:CGRectMake(10, 13, 8, 15)];
        [imgArrow setImage:[UIImage imageNamed:@"biaozhi.png"]];
        imgArrow.backgroundColor = [UIColor clearColor];
        [lab addSubview:imgArrow];
        
        Subapps *sub = [_arrSubDatail objectAtIndex:section+1];
        lab.text = [NSString stringWithFormat:@"     %@",sub.strSubappName];
        
        UIImageView *imgClass = [[UIImageView alloc]initWithFrame:CGRectMake(kScreenWidth-30, 14, 10, 18)];
        [imgClass setImage:[UIImage imageNamed:@"tuijianfl.png"]];
        imgClass.backgroundColor = [UIColor clearColor];
        [lab addSubview:imgClass];
        return _viewTitleBg;
    }
    else
    {
        UIButton *btnClass = [[UIButton alloc]initWithFrame:CGRectMake(0, 180, kScreenWidth, 40)];
        [btnClass setUserInteractionEnabled:YES];
        [btnClass setBackgroundColor:[UIColor clearColor]];
        btnClass.tag = 210+section;
        [btnClass addTarget:self action:@selector(clickClass:) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 40)];
        lab.backgroundColor = [UIColor clearColor];
        lab.font = [UIFont boldSystemFontOfSize:18.0f];
        [lab setUserInteractionEnabled:NO];
        Subapps *sub = [_arrSubDatail objectAtIndex:section+1];
        lab.text = [NSString stringWithFormat:@"     %@",sub.strSubappName];
        [btnClass addSubview:lab];
        
        UIView *viewLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.5)];
        viewLine.backgroundColor = [UIColor colorWithWhite:0.656 alpha:1.000];
        [lab addSubview:viewLine];
        
        //蓝色标识
        UIImageView *imgArrow = [[UIImageView alloc]initWithFrame:CGRectMake(10, 13, 8, 15)];
        [imgArrow setImage:[UIImage imageNamed:@"biaozhi.png"]];
        imgArrow.backgroundColor = [UIColor clearColor];
        [lab addSubview:imgArrow];
        
        UIImageView *imgClass = [[UIImageView alloc]initWithFrame:CGRectMake(kScreenWidth-30, 14, 10, 18)];
        [imgClass setImage:[UIImage imageNamed:@"tuijianfl.png"]];
        imgClass.backgroundColor = [UIColor clearColor];
        [lab addSubview:imgClass];
        
        return btnClass;
    }
}

//设置标题高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 220;
    }
    return 40;
}

//尾部标题的高度
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

//列表每行显示的内容
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int section = [indexPath section];
    NSString *identifierring = [NSString stringWithFormat:@"firstTable%d",section];
    //已经加载
    if ([_arrCell objectForKey:identifierring] && [[_arrCell objectForKey:identifierring] isKindOfClass:[UITableViewCell class]]) {
        UITableViewCell *cell = [_arrCell objectForKey:identifierring];
        return cell;
    }
    //获取cell row ＝ 0 获取不到 进入创建cell
    UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]init];//WithStyle: UITableViewCellStyleDefault reuseIdentifier:identifier];
        [cell setBackgroundColor:[UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSMutableArray *arrResult = [_arrVoide objectAtIndex:section];
    VideoInfoView *viewInfo = [[VideoInfoView alloc]initWithArrVideo:arrResult];
    viewInfo.frame = CGRectMake(0, 0, kScreenWidth, 167*2);
    viewInfo.delegate = self;
    [cell addSubview:viewInfo];
    [_arrCell setObject:cell forKey:identifierring];
    return cell;
}

//列表每行的高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 167*2;
}

//自定义cell的协议方法
-(void)videoInfo:(VideoInfoView *)videoView VideoInfo:(VideoInfo *)videoInfo
{
    DetailViewController *detail = [[DetailViewController alloc]initWithResId:videoInfo.strVideoId];
    detail.strTitle = videoInfo.strTitle;
    detail.strIndex = @"1";
    detail.strSource = @"";
    [self.navigationController pushViewController:detail animated:YES];
}

//推荐分类的按钮
-(void)clickClass:(UIButton *)sender
{
    NSMutableArray *arr = [_arrVoide objectAtIndex:sender.tag-210];
    VideoInfo *vIf = [arr objectAtIndex:0];
    VideoTableViewController *videoTable = [[VideoTableViewController alloc]initWithClassInfo:vIf];
    [self.navigationController pushViewController:videoTable animated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
