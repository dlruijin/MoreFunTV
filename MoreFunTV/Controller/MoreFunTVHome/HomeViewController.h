//
//  HomeViewController.h
//  MoreFunTV
//
//  Created by admin on 14-10-21.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "DetailViewController.h"
#import "VideoInfoView.h"
#import "NetWorkingHelper.h"
#import "SetUpSingleton.h"
#import "ClientXMLViewController.h"
#import "Loading.h"
#import "VideoTableViewController.h"
#import "AFNetworking.h"

#import "MatchstickDeviceController.h"
#import <Matchstick/Flint.h>

@interface HomeViewController : BaseViewController<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,VideoInfoViewDelegate,MatchstickControllerDelegate>
{
    UIScrollView *_scrollViewHot;
    UIPageControl *_pageControl;
    //推荐视频信息列表
    UITableView *_tableRecommend;
    NSMutableArray *_arrVoide;//视频信息数组
    NSMutableArray *_arrRec;//推荐大图信息
    NSMutableArray *_arrSubDatail;//推荐分类信息
    UILabel *_labTitle;//图片标题
    //标题上的背景图片
    UIView *_viewTitleBg;
    UIView *_viel;
    NSString *_setStrApiURL;
    Client *_client;
    NSMutableArray *_arr;
    //存储cell
    NSMutableDictionary *_arrCell;
}
@end
