//
//  LiveTableViewCell.h
//  MoreFunTV
//
//  Created by admin on 14-10-24.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"

@interface LiveTableViewCell : UITableViewCell
@property(nonatomic,retain) UIImageView *img;//节目图片
@property(nonatomic,retain) UILabel *labTvName;//电视台
@property(nonatomic,retain) UILabel *labNowTheatre;//正在播放的剧场
@property(nonatomic,retain) UILabel *labWillTheatre;//即将播放的剧场
@end
