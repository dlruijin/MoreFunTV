//
//  LiveTableViewController.h
//  MoreFunTV
//
//  Created by admin on 14-10-23.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "LiveTableViewCell.h"
#import "NetWorkingHelper.h"
#import "DetailViewController.h"
#import "Loading.h"
#import "MJRefresh.h"
#import "SafeCheckParameter.h"
#import "WebviewViewController.h"
#import "MatchstickDeviceController.h"
#import "MatchStickViewController.h"
#import "AlertCustomView.h"

@interface LiveTableViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,MatchstickControllerDelegate,UIAlertViewDelegate,AlertCustomViewDelegate>
{
    UIView *_viewB;//活动的小条
    UITableView    *_tableLive;//视频信息列表
    NSMutableArray *_arrId;//视频Id数组
    NSMutableArray *_arrLive;//视频详情数组
    RequestParameter *_rep;//请求参数
    DetailInfo *_detail;
    NSString  *_strUrlWebView;//网页地址
    NSString  *_strRUrl;//投射地址
    NSMutableArray *_arrSheet;
    NSString *_strRealUrlAll;
    NSDictionary *_urlDic;
    BOOL _alertFlag;
    NSArray *_arr1;
    NSArray *_arr2;
    NSArray *_arr3;
    NSArray *_arr4;
    BOOL _pushmat;
    AlertCustomView *_cusAlterView;
}
@property(nonatomic,retain) VideoInfo *classInfo;
- (id)initWithClassInfo:(VideoInfo *)classInfo;
@end
