//
//  UIWebView+VideoControl.h
//  MoreFunTV
//
//  Created by Dlrj on 15-1-15.
//  Copyright (c) 2015年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWebView (VideoControl)


- (BOOL)hasVideo;
- (NSString *)getVideoTitle;
- (double)getVideoDuration;
- (double)getVideoCurrentTime;

- (int)play;
- (int)pause;
- (int)resume;
- (int)stop;

@end