// Copyright (C) 2013-2014, Infthink (Beijing) Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#import "MatchstickDeviceController.h"
#import "SimpleImageFetcher.h"
#import <Matchstick/Flint.h>
#import "SetUpSingleton.h"
#import "DBDaoHelper.h"

static NSString *kReceiverAppID;  //Replace with your app id
static NSString *kReceiverAppURL; //Receiver app url

@interface MatchstickDeviceController () {
    MatchstickControllerFeatures _features;
    UIImage *_btnImage;
    UIImage *_btnImageConnected;
    dispatch_queue_t _queue;
}

@property MSFKMediaControlChannel *mediaControlChannel;
@property MSFKApplicationMetadata *applicationMetadata;
@property MSFKDevice *selectedDevice;

@property bool deviceMuted;
@property(nonatomic) VolumeChangeController *volumeChangeController;
@property(nonatomic) NSArray *idleStateToolbarButtons;
@property(nonatomic) NSArray *playStateToolbarButtons;
@property(nonatomic) NSArray *pauseStateToolbarButtons;
@property(nonatomic) UIImageView *toolbarThumbnailImage;
@property(nonatomic) NSURL *toolbarThumbnailURL;
@property(nonatomic) UILabel *toolbarTitleLabel;
@property(nonatomic) UILabel *toolbarSubTitleLabel;
@property(nonatomic) BOOL selectingDeviceSys;
@end

@implementation MatchstickDeviceController

- (id)init {
    id controller = [self initWithFeatures:MatchstickControllerFeaturesNone];
    NSNotificationCenter *notificationDismiss = [NSNotificationCenter defaultCenter];
    [notificationDismiss addObserver:controller selector:@selector(selectingDevices) name:@"selectingDevice" object:nil];
    return controller;
}

- (void)selectingDevices {
    self.selectingDeviceSys = NO;
}

- (void)selectingDevice {
    self.selectingDeviceSys = YES;
}

- (id)initWithFeatures:(MatchstickControllerFeatures)featureFlags {
    self = [super init];
    if (self) {
        kReceiverAppURL = @"http://openflint.github.io/flint-player/player.html";
        // Remember the features.
        _features = featureFlags;
        
        // Init volume change controller if its requested.
        if (featureFlags & MatchstickControllerFeatureHWVolumeControl) {
            self.volumeChangeController = [[VolumeChangeController alloc] init];
            self.volumeChangeController.delegate = self;
        }
        
        // Initialize device scanner
        self.deviceScanner = [[MSFKDeviceScanner alloc] init];
        
        // Initialize UI controls for navigation bar and tool bar.
        [self initControls];
        
        _queue = dispatch_queue_create("tv.matchstick.sample.Fling", NULL);
        
    }
    return self;
}

- (BOOL)isConnected {
    return self.deviceManager.isConnected;
}

- (BOOL)isPlayingMedia
{
    return self.deviceManager.isConnected && self.mediaControlChannel &&
    self.mediaControlChannel.mediaStatus && (self.playerState == MSFKMediaPlayerStatePlaying ||self.playerState == MSFKMediaPlayerStatePaused ||self.playerState == MSFKMediaPlayerStateBuffering);
}


- (float) getStreamVolume
{
    return self.mediaControlChannel.mediaStatus.volume;
}

- (void) setStreamVolume : (float) streamVolume
{
    [self.mediaControlChannel setStreamVolume:streamVolume];
}

- (void)performScan:(BOOL)start {
    
    if (start) {
        [self.deviceScanner addListener:self];
        [self.deviceScanner startScan];
    } else {
        [self.deviceScanner stopScan];
        [self.deviceScanner removeListener:self];
    }
}
- (void)connectToDevice:(MSFKDevice *)device {
    NSLog(@"Device address: %@:%d", device.ipAddress, (unsigned int) device.servicePort);
    self.selectedDevice = device;
    
    NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
    NSString *appIdentifier = [info objectForKey:@"CFBundleIdentifier"];
    self.deviceManager =
    [[MSFKDeviceManager alloc] initWithDevice:self.selectedDevice
                           clientPackageName:appIdentifier];
    self.deviceManager.delegate = self;
    self.deviceManager.appId = @"~flintplayer";
    //kReceiverAppID = @"E9C6F80E"; //Matchstick APP ID also allowed
//    kReceiverAppID = [self.deviceManager makeApplicationId:kReceiverAppURL];
    kReceiverAppID = kReceiverAppURL;
    NSLog(@"kReceiverAppID %@",kReceiverAppID);

    [self.deviceManager connect];
    
    // Start animating the fling connect images.
    UIButton *matchstickButton = (UIButton *) self.matchstickBarButton.customView;
    matchstickButton.tintColor = [UIColor whiteColor];
    matchstickButton.imageView.animationImages =
    @[[UIImage imageNamed:@"coning1.png"],
      [UIImage imageNamed:@"coning2.png"],
      [UIImage imageNamed:@"coning3.png"],
      [UIImage imageNamed:@"coning2.png"],
      [UIImage imageNamed:@"coning1.png"]];
    matchstickButton.imageView.animationDuration = 2;
    [matchstickButton.imageView startAnimating];
}

- (void)disconnectFromDevice {
    NSLog(@"Disconnecting device:%@", self.selectedDevice.friendlyName);
    // New way of doing things: We're not going to stop the applicaton. We're just going
    // to leave it.
    [self.deviceManager leaveApplication];
    // If you want to force application to stop, uncomment below
    //[self.deviceManager stopApplication];
    [self.deviceManager disconnect];
}

- (void)updateToolbarForViewController:(UIViewController *)viewController {
    [self updateToolbarStateIn:viewController];
}

- (void)updateStatsFromDevice {
    if (self.isConnected && self.mediaControlChannel && self.mediaControlChannel.mediaStatus) {
        NSLog(@"updateInterfaceFromFling1");
        _streamPosition = [self.mediaControlChannel approximateStreamPosition];
        _streamDuration = self.mediaControlChannel.mediaStatus.mediaInformation.streamDuration;
        NSLog(@"%f",_streamDuration);
        _playerState = self.mediaControlChannel.mediaStatus.playerState;
        _mediaInformation = self.mediaControlChannel.mediaStatus.mediaInformation;
    }
}

-(BOOL) isPlayMediaFinish {
    return self.mediaControlChannel.mediaStatus.idleReason == MSFKMediaPlayerIdleReasonFinished;
}


-(void)setDeviceVolume:(float)deviceVolume
{
    [self.deviceManager setVolume:deviceVolume];
}

- (void)changeVolumeIncrease:(BOOL)goingUp {
    float idealVolume = self.getStreamVolume + (goingUp ? 0.1 : -0.1);
    idealVolume = MIN(1.0, MAX(0.0, idealVolume));
    [self setStreamVolume:idealVolume];
}

- (void)setPlaybackPercent:(float)newPercent {
    newPercent = MAX(MIN(1.0, newPercent), 0.0);
    
    NSTimeInterval newTime = newPercent * _streamDuration;
    if (_streamDuration > 0 && self.isConnected) {
        [self.mediaControlChannel seekToTimeInterval:newTime];
    }
}

- (void)pauseFlingMedia:(BOOL)shouldPause {
    if (self.isConnected && self.mediaControlChannel && self.mediaControlChannel.mediaStatus) {
        if (shouldPause) {
            [self.mediaControlChannel pause];
        } else {
            [self.mediaControlChannel play];
        }
    }
}

-(void)stopApplication {
    [self.deviceManager stopApplication];
    [self.deviceManager disconnect];
}

- (void)stopFlingMedia {
    if (self.isConnected && self.mediaControlChannel && self.mediaControlChannel.mediaStatus) {
        NSLog(@"Telling fling media control channel to stop");
        [self.mediaControlChannel stop];
    }
}

#pragma mark - MSFKDeviceManagerDelegate

- (void)deviceManagerDidConnect:(MSFKDeviceManager *)deviceManager {
    NSLog(@"连接1");
    NSLog(@"connected!!");
    NSLog(@"kReceiverAppID %@",kReceiverAppID);
    if (self.selectingDeviceSys) {
        if (![self.deviceManager launchApplication:kReceiverAppID
                                 relaunchIfRunning:NO]) {
            //Application 失败的提示：：
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:MyLocalizedString(@"alertinfo1") message:MyLocalizedString(@"alertinfoh") delegate:nil cancelButtonTitle:MyLocalizedString(@"sure") otherButtonTitles:nil];
            [alert show];
        }
    }
    else {
        if (![self.deviceManager launchApplication:kReceiverAppID
                                 relaunchIfRunning:NO]) {
            //Application 失败的提示：：
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:MyLocalizedString(@"alertinfo1") message:MyLocalizedString(@"alertinfoh") delegate:nil cancelButtonTitle:MyLocalizedString(@"sure") otherButtonTitles:nil];
            [alert show];
        }
        self.selectingDeviceSys = YES;
    }
}

- (void)      deviceManager:(MSFKDeviceManager *)deviceManager
didConnectToFlintApplication:(MSFKApplicationMetadata *)applicationMetadata
        launchedApplication:(BOOL)launchedApplication {
    NSLog(@"连接3");
    NSLog(@"application has launched");
    self.mediaControlChannel = [[MSFKMediaControlChannel alloc] init];
    self.mediaControlChannel.delegate = self;
    [self.deviceManager addChannel:self.mediaControlChannel];
    [self.mediaControlChannel requestStatus];
    
    self.applicationMetadata = applicationMetadata;
    [self updateFlingIconButtonStates];
    
    if ([self.delegate respondsToSelector:@selector(didConnectToDevice:)]) {
        [self.delegate didConnectToDevice:self.selectedDevice];
    }
    
    // Hook to hardware volume controls.
    if (_features & MatchstickControllerFeatureHWVolumeControl) {
        [self.volumeChangeController captureVolumeButtons];
    }
}

//Application失败
- (void)                 deviceManager:(MSFKDeviceManager *)deviceManager
didFailToConnectToApplicationWithError:(NSError *)error {
    if (error != nil) {
        [self showError:error];
    }
    NSLog(@"error:%@",error);
    //Application 失败的提示：：
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:MyLocalizedString(@"alertinfo1") message:MyLocalizedString(@"alertinfoh") delegate:nil cancelButtonTitle:MyLocalizedString(@"sure") otherButtonTitles:nil];
//    [alert show];
    UIButton *matchstickButton = (UIButton *) self.matchstickBarButton.customView;
    [matchstickButton.imageView stopAnimating];
    [self deviceDisconnected];
    [self updateFlingIconButtonStates];
    // Hook to hardware volume controls.
    if (_features & MatchstickControllerFeatureHWVolumeControl) {
        [self.volumeChangeController releaseVolumeButtons];
    }
}

//
- (void)                deviceManager:(MSFKDeviceManager *)deviceManager
didDisconnectFromApplicationWithError:(NSError *)error
{
    if (error != nil) {
        [self showError:error];
    }
    UIButton *matchstickButton = (UIButton *) self.matchstickBarButton.customView;
    [matchstickButton.imageView stopAnimating];
    
    [self deviceDisconnected];
    [self updateFlingIconButtonStates];
    // Hook to hardware volume controls.
    if (_features & MatchstickControllerFeatureHWVolumeControl) {
        [self.volumeChangeController releaseVolumeButtons];
    }
}

//设备连接失败
- (void)    deviceManager:(MSFKDeviceManager *)deviceManager
didFailToConnectWithError:(MSFKError *)error {
//    if (error != nil) {
//        [self showError:error];
//    }
    //设备连接失败的提示：
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:MyLocalizedString(@"alertinfo1") message:MyLocalizedString(@"alertinfog") delegate:nil cancelButtonTitle:MyLocalizedString(@"sure") otherButtonTitles:nil];
    [alert show];
    UIButton *matchstickButton = (UIButton *) self.matchstickBarButton.customView;
    [matchstickButton.imageView stopAnimating];
    [self deviceDisconnected];
    [self updateFlingIconButtonStates];
    // Hook to hardware volume controls.
    if (_features & MatchstickControllerFeatureHWVolumeControl) {
        [self.volumeChangeController releaseVolumeButtons];
    }
}

//自动断开连接
- (void)deviceManager:(MSFKDeviceManager *)deviceManager didDisconnectWithError:(MSFKError *)error {
    NSLog(@"Received notification that device disconnected");
    if (error != nil) {
        [self showError:error];
    }
    else {
        NSLog(@"__________didDisconnectWithError(底层通知)error=nil");
    }
    UIButton *matchstickButton = (UIButton *) self.matchstickBarButton.customView;
    [matchstickButton.imageView stopAnimating];
    
    [self deviceDisconnected];
    [self updateFlingIconButtonStates];
    
}

- (void) deviceManager:(MSFKDeviceManager *)deviceManager
volumeDidChangeToLevel:(float)volumeLevel
               isMuted:(BOOL)isMuted {
    NSLog(@"连接2");
    NSLog(@"New volume level of %f reported!", volumeLevel);
    //    self.deviceVolume = volumeLevel;
    self.deviceMuted = isMuted;
    _deviceVolume = volumeLevel;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Volume changed" object:self];
}


- (void)deviceDisconnected {
    NSLog(@"设备断开连接1");
    //隐藏浮动按钮
    SetUpSingleton *set = [SetUpSingleton sharedSetUp];
    set.strHidden = @"YES";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BtnSetHidden" object:nil userInfo:nil];
    
    self.mediaControlChannel = nil;
    self.deviceManager = nil;
    self.selectedDevice = nil;
    
    if ([self.delegate respondsToSelector:@selector(didDisconnect)]) {
        [self.delegate didDisconnect];
    }
}

#pragma mark - MSFKDeviceScannerListener
- (void)deviceDidComeOnline:(MSFKDevice *)device {
    NSLog(@"device found!! %@ ... %@", device.friendlyName,device.ipAddress);
    [self updateFlingIconButtonStates];
    if(self.delegate) {
        if ([self.delegate respondsToSelector:@selector(didDiscoverDeviceOnNetwork)]) {
            [self.delegate didDiscoverDeviceOnNetwork];
        }
    }
}

- (void)deviceDidGoOffline:(MSFKDevice *)device {
    NSLog(@"__________deviceDidGoOffline(底层通知)");
    [self updateFlingIconButtonStates];
}

#pragma - MSFKMediaControlChannelDelegate methods


- (void) mediaControlChannel:(MSFKMediaControlChannel *)mediaControlChannel
didCompleteLoadWithSessionID:(NSInteger)sessionID {
    _mediaControlChannel = mediaControlChannel;
}

- (void)mediaControlChannelDidUpdateStatus:(MSFKMediaControlChannel *)mediaControlChannel {
    [self updateStatsFromDevice];
    NSLog(@"mediaControlChannelDidUpdateStatus Media control channel status changed");
    _mediaControlChannel = mediaControlChannel;
    if ([self.delegate respondsToSelector:@selector(didReceiveMediaStateChange)]) {
        [self.delegate didReceiveMediaStateChange];
    }
}

- (void)mediaControlChannelDidUpdateMetadata:(MSFKMediaControlChannel *)mediaControlChannel {
    NSLog(@" mediaControlChannelDidUpdateMetadata Media control channel metadata changed");
        _mediaControlChannel = mediaControlChannel;
        [self updateStatsFromDevice];
        
        if ([self.delegate respondsToSelector:@selector(didReceiveMediaStateChange)]) {
            [self.delegate didReceiveMediaStateChange];
        }
}

- (BOOL)loadMedia:(NSURL *)url
     thumbnailURL:(NSURL *)thumbnailURL
            title:(NSString *)title
         subtitle:(NSString *)subtitle
         mimeType:(NSString *)mimeType
        startTime:(NSTimeInterval)startTime
         autoPlay:(BOOL)autoPlay {
    if (!self.deviceManager || !self.deviceManager.isConnected) {
        return NO;
    }
    
    MSFKMediaMetadata *metadata = [[MSFKMediaMetadata alloc] init];
    if (title) {
        [metadata setString:title forKey:kMSFKMetadataKeyTitle];
    }
    
    if (subtitle) {
        [metadata setString:subtitle forKey:kMSFKMetadataKeySubtitle];
    }
    
    if (thumbnailURL) {
        [metadata addImage:[[MSFKImage alloc] initWithURL:thumbnailURL width:200 height:100]];
    }
    
    MSFKMediaInformation *mediaInformation =
    [[MSFKMediaInformation alloc] initWithContentID:[url absoluteString]
                                         streamType:MSFKMediaStreamTypeNone
                                        contentType:mimeType
                                           metadata:metadata
                                     streamDuration:0
                                         customData:nil];
    [self.mediaControlChannel loadMedia:mediaInformation autoplay:autoPlay playPosition:startTime];
    
    return YES;
}

#pragma mark - VolumeChangeControllerDelegate
- (void)didChangeVolumeUp {
    [self changeVolumeIncrease:YES];
}

- (void)didChangeVolumeDown {
    [self changeVolumeIncrease:NO];
}

#pragma mark - implementation

- (void)showError:(NSError *)error {
//    NSLog(@"错误信息：......*******************************************************************************************************************************************************************************************************************..............%@", [MSFKError enumDescriptionForCode:[error code]]);
//    NSLog(@"错误信息Code：......*******************************************************************************************************************************************************************************************************************..............%d",[error code]);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (error != nil){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:MyLocalizedString(@"alertinfo1") message:MyLocalizedString(@"alertinfoc") delegate:self cancelButtonTitle:MyLocalizedString(@"sure") otherButtonTitles:nil];
            [alert show];
        }
    });
}

//弹出框按钮的事件
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    SetUpSingleton *set = [SetUpSingleton sharedSetUp];
    [DBDaoHelper deleteAllTouShe];
    set.count = 0;
    set.arrCount = 0;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dismiss" object:nil userInfo:nil];
    [self stopFlingMedia];
    [self stopApplication];
    [self disconnectFromDevice];
    [set.arrSheet removeAllObjects];
}


- (NSString *)getDeviceName {
    if (self.selectedDevice == nil)
        return @"";
    return self.selectedDevice.friendlyName;
}

- (void)initControls {
    // Create Matchstick bar button.
    _btnImage = [UIImage imageNamed:@"coning1"];
    _btnImageConnected = [UIImage
                          imageNamed:@"coning4.png"];
    
    UIButton *matchstickButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [matchstickButton addTarget:self
                         action:@selector(chooseDevice:)
               forControlEvents:UIControlEventTouchDown];
    matchstickButton.frame = CGRectMake(0, 0, _btnImage.size.width, _btnImage.size.height);
    [matchstickButton setTintColor:[UIColor whiteColor]];
    [matchstickButton setBackgroundImage:_btnImage forState:UIControlStateNormal];
    matchstickButton.hidden = YES;
    
    _matchstickBarButton = [[UIBarButtonItem alloc] initWithCustomView:matchstickButton];
   
    
    // Create toolbar buttons for the mini player.
    CGRect frame = CGRectMake(0, 0, 49, 37);
    _toolbarThumbnailImage =
    [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"video_thumb_mini.png"]];
    _toolbarThumbnailImage.frame = frame;
    _toolbarThumbnailImage.contentMode = UIViewContentModeScaleAspectFit;
    UIButton *someButton = [[UIButton alloc] initWithFrame:frame];
    [someButton addSubview:_toolbarThumbnailImage];
    [someButton addTarget:self
                   action:@selector(showMedia)
         forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    UIBarButtonItem *thumbnail = [[UIBarButtonItem alloc] initWithCustomView:someButton];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 200, 45)];
    _toolbarTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 185, 30)];
    _toolbarTitleLabel.backgroundColor = [UIColor clearColor];
    _toolbarTitleLabel.font = [UIFont systemFontOfSize:17];
    _toolbarTitleLabel.text = @"This is the title";
    _toolbarTitleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _toolbarTitleLabel.textColor = [UIColor blackColor];
    [btn addSubview:_toolbarTitleLabel];
    
    _toolbarSubTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, 185, 30)];
    _toolbarSubTitleLabel.backgroundColor = [UIColor clearColor];
    _toolbarSubTitleLabel.font = [UIFont systemFontOfSize:14];
    _toolbarSubTitleLabel.text = @"This is the sub";
    _toolbarSubTitleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _toolbarSubTitleLabel.textColor = [UIColor grayColor];
    [btn addSubview:_toolbarSubTitleLabel];
    [btn addTarget:self action:@selector(showMedia) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *titleBtn = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    UIBarButtonItem *flexibleSpaceLeft =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                  target:nil
                                                  action:nil];
    
    UIBarButtonItem *playButton =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemPlay
                                                  target:self
                                                  action:@selector(playMedia)];
    playButton.tintColor = [UIColor blackColor];
    
    UIBarButtonItem *pauseButton =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemPause
                                                  target:self
                                                  action:@selector(pauseMedia)];
    pauseButton.tintColor = [UIColor blackColor];
    
    _idleStateToolbarButtons = [NSArray arrayWithObjects:thumbnail, titleBtn, flexibleSpaceLeft, nil];
    _playStateToolbarButtons =
    [NSArray arrayWithObjects:thumbnail, titleBtn, flexibleSpaceLeft, pauseButton, nil];
    _pauseStateToolbarButtons =
    [NSArray arrayWithObjects:thumbnail, titleBtn, flexibleSpaceLeft, playButton, nil];
}

- (void)chooseDevice:(id)sender {
    if ([self.delegate respondsToSelector:@selector(shouldDisplayModalDeviceController)]) {
        [_delegate shouldDisplayModalDeviceController];
    }
}

- (void)updateFlingIconButtonStates {
    // Hide the button if there are no devices found.
    UIButton *matchstickButton = (UIButton *) self.matchstickBarButton.customView;
    dispatch_async(dispatch_get_main_queue(), ^{
        //主线程更新UI
        if (self.deviceScanner.devices.count == 0) {
            matchstickButton.hidden = YES;
        } else {
            matchstickButton.hidden = NO;
            if (self.deviceManager && self.deviceManager.isConnected)
            {
                [matchstickButton.imageView stopAnimating];
                
                SetUpSingleton *set = [SetUpSingleton sharedSetUp];
                //不在投射播放
                if ([set.strHidden isEqualToString:@"YES"]) {
                    // Hilight with yellow tint color.
                    [matchstickButton setTintColor:[UIColor whiteColor]];
                    [matchstickButton setImage:_btnImageConnected forState:UIControlStateNormal];
//                    NSLog(@"______________btnImageConnected");
                }
//                NSLog(@"______________btnImageConnected_playing");
            }
            else
            {
                // Remove the hilight.
                [matchstickButton setTintColor:[UIColor whiteColor]];
                [matchstickButton setImage:_btnImage forState:UIControlStateNormal];
//                NSLog(@"_______________btnImage");
            }
        }
    });
}

- (void)updateToolbarStateIn:(UIViewController *)viewController {
    // Ignore this view controller if it is not visible.
    if (!(viewController.isViewLoaded && viewController.view.window)) {
        return;
    }

    // Get the playing status.
    if (self.isPlayingMedia) {
        viewController.navigationController.toolbarHidden = YES;
    } else {
        viewController.navigationController.toolbarHidden = YES;
        return;
    }
    // Update the play/pause state.
    if (self.playerState == MSFKMediaPlayerStateUnknown ||
        self.playerState == MSFKMediaPlayerStateIdle) {
        viewController.toolbarItems = self.idleStateToolbarButtons;
    } else {
        
        BOOL playing = (self.playerState == MSFKMediaPlayerStatePlaying ||
                        self.playerState == MSFKMediaPlayerStateBuffering);
        if (playing) {
            viewController.toolbarItems = self.playStateToolbarButtons;
            
        } else {
            viewController.toolbarItems = self.pauseStateToolbarButtons;
        }
    }
    
    // Update the title.
    self.toolbarTitleLabel.text = [self.mediaInformation.metadata stringForKey:kMSFKMetadataKeyTitle];
    self.toolbarSubTitleLabel.text =
    [self.mediaInformation.metadata stringForKey:kMSFKMetadataKeySubtitle];
    
    if(self.mediaInformation.metadata.images.count > 0) {
        // Update the image.
        MSFKImage *img = [self.mediaInformation.metadata.images objectAtIndex:0];
        
        if ([img.URL isEqual:self.toolbarThumbnailURL]) {
            return;
        }
    //Loading thumbnail async
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *image = [UIImage imageWithData:[SimpleImageFetcher getDataFromImageURL:img.URL]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Loaded thumbnail image");
            self.toolbarThumbnailURL = img.URL;
            self.toolbarThumbnailImage.image = image;
        });
    });
}
}

- (void)playMedia {
    [self pauseFlingMedia:NO];
}

- (void)pauseMedia {
    [self pauseFlingMedia:YES];
}

- (void)showMedia {
    if ([self.delegate respondsToSelector:@selector(shouldPresentPlaybackController)]) {
        [self.delegate shouldPresentPlaybackController];
    }
}
@end
