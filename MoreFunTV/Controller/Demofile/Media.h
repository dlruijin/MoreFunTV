//
//  Media.h
//  MoreFunTV
//
//  Created by admin on 2014-11-2.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#define MEDIA_URL_BASE @"http://myfirefly.s3.amazonaws.com/cast/droidream/samples/"
#define MEDIA_URL_FILE @"vlist.json"
@interface Media : NSObject
@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *descrip;
@property(nonatomic, copy) NSString *mimeType;
@property(nonatomic, copy) NSString *subtitle;
@property(nonatomic, strong) NSURL *URL;
@property(nonatomic, strong) NSURL *thumbnailURL;
@property(nonatomic, strong) NSURL *posterURL;

+ (id)mediaFromExternalJSON:(NSDictionary *)jsonAsDict;

@end
