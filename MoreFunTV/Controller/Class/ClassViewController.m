//
//  ClassViewController.m
//  MoreFunTV
//
//  Created by admin on 14-10-21.
//  Copyright (c) 2014????? admin. All rights reserved.
//

#import "ClassViewController.h"
#import "AppDelegate.h"
#import "UIImageView+AFNetworking.h"

@interface ClassViewController ()
{
    __weak MatchstickDeviceController *_matchstickDeviceController;
}
@end

@implementation ClassViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    AppDelegate *delegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
    _matchstickDeviceController = delegate.matchstickDeviceController;
    //投射图标判断
    if (_matchstickDeviceController.deviceScanner.devices.count > 0) {
        self.tabBarController.navigationItem.rightBarButtonItem = _matchstickDeviceController.matchstickBarButton;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //TiTle自定义
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 60, 44)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:19.0f];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = MyLocalizedString(@"fenlei");
    self.tabBarController.navigationItem.titleView = titleLabel;
    
    _matchstickDeviceController.delegate = self;

    _flag = 0;
    [Loading showLoadingWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //执行耗时操作
        _arrClass = [NetWorkingHelper getClassInfoWith:@"categories"];
        dispatch_async(dispatch_get_main_queue(), ^{
            //主线程更新UI
            [Loading hiddonLoadingWithView:self.view];
            
            for (UIButton *btn  in [_scrollViewClass subviews])
            {
                [btn removeFromSuperview];
            }
            //添加分类信息
            [self addTableInfo];
        });
    });
}

- (void)shouldDisplayModalDeviceController {
    //投射按钮调用的方法
    [[NSNotificationCenter defaultCenter] postNotificationName:@"myDevice" object:nil userInfo:nil];
}

- (void)didDiscoverDeviceOnNetwork {
    // Add the Matchstick icon if not present.
    self.tabBarController.navigationItem.rightBarButtonItem = _matchstickDeviceController.matchstickBarButton;
}

//添加分类信息
-(void)addTableInfo
{
    UIView *bView = [[UIView alloc]init];
    [self.view addSubview:bView];
    
    _scrollViewClass = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, kScreenWidth, kScreenHeight-64-48)];
    //设置滚动区域 如果比坐标设置的宽高小将不能滚动
    _scrollViewClass.contentSize = CGSizeMake(kScreenWidth,kScreenHeight-64-48);
    _scrollViewClass.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000];
    //整页屏滚动
    _scrollViewClass.pagingEnabled = NO;
    //设置是否显示滚条
    _scrollViewClass.showsVerticalScrollIndicator = NO;
    _scrollViewClass.showsHorizontalScrollIndicator = NO;
    _scrollViewClass.bounces = NO;
    [self.view addSubview:_scrollViewClass];
    
    
    if (_arrClass.count%3 > 0)
    {
        _bCount = _arrClass.count/3+1;
    }
    else
    {
        _bCount = _arrClass.count/3;
    }
    
    for (int i = 0; i < _bCount; i++)
    {
        if (i == _bCount-1 && _arrClass.count%3 > 0)
        {
            _aCount = _arrClass.count%3;
        }
        else
        {
            _aCount = 3;
        }
        
        //影片信息按钮
        for (int j = 0; j < _aCount; j++)
        {//更改按钮数量。。在此处获取接口数据为按钮赋值
            
            VideoInfo *vInfo = [_arrClass objectAtIndex:_flag];
            UIButton *btnClass = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            btnClass.frame = CGRectMake(kScreenWidth/3*j, 80*i, kScreenWidth/3, 80);
            btnClass.tag = 122 +_flag;
            _flag++;
            btnClass.clipsToBounds = YES;
            [btnClass setBackgroundColor:[UIColor clearColor]];
            [btnClass addTarget:self action:@selector(clickClass:) forControlEvents:UIControlEventTouchUpInside];
            [_scrollViewClass addSubview:btnClass];
            
            //竖边线
            UIView *viewH = [[UIView alloc]initWithFrame:CGRectMake(kScreenWidth/3-0.5, 0, 0.5, 80)];
            viewH.backgroundColor = [UIColor colorWithWhite:0.656 alpha:1.000];
            [btnClass addSubview:viewH];
            
            //横边线
            UIView *viewW = [[UIView alloc]initWithFrame:CGRectMake(0, 80-0.5, kScreenWidth/3, 0.5)];
            viewW.backgroundColor = [UIColor colorWithWhite:0.656 alpha:1.000];
            [btnClass addSubview:viewW];
            
            //分类图标
            UIImageView *imgClass = [[UIImageView alloc]init];
            [imgClass setImageWithURL:[NSURL URLWithString:vInfo.strCategoryLog] placeholderImage:[UIImage imageNamed:@"app_default.png"]];
            imgClass.frame = CGRectMake((kScreenWidth/3-35)/2, 15, 35, 35);
            if ([vInfo.strCategoryId intValue] == 1 || [vInfo.strCategoryId intValue] == 4) {
                imgClass.frame = CGRectMake((kScreenWidth/3-41)/2, 15, 41, 35);
            }
            imgClass.backgroundColor = [UIColor clearColor];
            [btnClass addSubview:imgClass];
            
            //影片概要
            UILabel *labClassName = [[UILabel alloc]initWithFrame:CGRectMake(0, 80-25, kScreenWidth/3, 25)];
            labClassName.backgroundColor = [UIColor clearColor];
            labClassName.font = [UIFont systemFontOfSize:16.0f];
            labClassName.textColor = [UIColor colorWithWhite:0.075 alpha:1.000];
            labClassName.textAlignment = NSTextAlignmentCenter;
            labClassName.text = vInfo.strCategoryName;
            [btnClass addSubview:labClassName];
        }
    }
}

//分类按钮的点击事件
-(void)clickClass:(UIButton *)sender
{
    _class = [_arrClass objectAtIndex:sender.tag-122];
    if ([_class.strCategoryId intValue] == 6)
    {
        LiveTableViewController *liveTable = [[LiveTableViewController alloc]initWithClassInfo:_class];
        [self.navigationController pushViewController:liveTable animated:YES];
    }
    else
    {
        VideoTableViewController *videoTable = [[VideoTableViewController alloc]initWithClassInfo:_class];
        [self.navigationController pushViewController:videoTable animated:YES];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
