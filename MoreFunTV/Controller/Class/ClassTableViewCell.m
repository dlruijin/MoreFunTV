//
//  ClassTableViewCell.m
//  MoreFunTV
//
//  Created by admin on 14-10-21.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "ClassTableViewCell.h"

@implementation ClassTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        //分类名称
        self.labClassName = [[UILabel alloc]initWithFrame:CGRectMake(55, 0, 95, 45)];
        self.labClassName.backgroundColor = [UIColor whiteColor];
        self.labClassName.font = [UIFont systemFontOfSize:18.0f];
        self.labClassName.textColor = [UIColor colorWithWhite:0.075 alpha:1.000];
        self.labClassName.textAlignment = NSTextAlignmentLeft;
        [self addSubview:self.labClassName];
        
        self.viewLine = [[UIView alloc]init];
        self.viewLine.frame = CGRectMake(55 , 44, kScreenWidth-55, 1);
        self.viewLine.backgroundColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.000];
        [self addSubview:self.viewLine];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
