//
//  WebviewViewController.h
//  MoreFunTV
//
//  Created by admin on 2014-10-29.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "Loading.h"

@interface WebviewViewController : UIViewController<UIWebViewDelegate>
{
    UIWebView *_webView;
}
@property(nonatomic,retain) NSString *strURL;
-(id)initWithHtmlUrl:(NSString *) strurl;

@end
