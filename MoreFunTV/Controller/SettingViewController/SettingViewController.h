//
//  SettingViewController.h
//  MoreFunTV
//
//  Created by admin on 2014-10-24.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "AboutViewController.h"
#import "SetUpUdf.h"
#import "SetUpSingleton.h"
#import "MatchstickDeviceController.h"
#import <Matchstick/Flint.h>

@interface SettingViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    NSString *_cachPath;
    NSArray *_files;
    UILabel *_labCount;
}
@end
