//
//  AboutViewController.m
//  MoreFunTV
//
//  Created by admin on 14-11-1.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "AboutViewController.h"
#import "AppDelegate.h"

@interface AboutViewController ()
{
    __weak MatchstickDeviceController *_matchstickDeviceController;
}
@end

@implementation AboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithWhite:0.973 alpha:1.000];
    
    AppDelegate *delegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
    _matchstickDeviceController = delegate.matchstickDeviceController;
    //投射图标判断
    if (_matchstickDeviceController.deviceScanner.devices.count > 0) {
        self.navigationItem.rightBarButtonItem = _matchstickDeviceController.matchstickBarButton;
    }
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 60, 44)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:19.0f];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = MyLocalizedString(@"textinfo6");
    self.navigationItem.titleView = titleLabel;
    
    //标题自定义
    [self customNavTitle];
    
    //设置列表
    [self setUpView];
    
    //添加log图片
    [self imageLogo];
    
    
    _reslut = YES;
}
-(NSString *)banben
{
    NSDictionary* infoDict =[[NSBundle mainBundle] infoDictionary];
    NSString *versionNum =[infoDict objectForKey:@"CFBundleVersion"];
    NSString *appName =[infoDict objectForKey:@"CFBundleDisplayName"];
    NSString *text =[NSString stringWithFormat:@"%@ ---%@",appName,versionNum];
//    NSLog(@"应用名称和版本号：%@",text);
    return text;

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _matchstickDeviceController.delegate = self;
}

- (void)shouldDisplayModalDeviceController {
    //投射按钮调用的方法
    [[NSNotificationCenter defaultCenter] postNotificationName:@"myDevice" object:nil userInfo:nil];
}

- (void)didDiscoverDeviceOnNetwork {
    // Add the Matchstick icon if not present.
    self.navigationItem.rightBarButtonItem = _matchstickDeviceController.matchstickBarButton;
}

//标题自定义
-(void)customNavTitle
{
    UIButton *btnBack = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 40, 44)];
    btnBack.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    [btnBack setBackgroundColor:[UIColor clearColor]];
    [btnBack setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(btnBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIImageView *image = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"return.png"]];
    image.frame = CGRectMake(0, 12, 10, 20);
    [btnBack addSubview:image];
    
    
   }


-(void)setUpView
{
    UILabel *labText= [[UILabel alloc]init];
    labText.frame = CGRectMake(10, 75, kScreenWidth-20,80);
    labText.font = [UIFont systemFontOfSize:13];
    labText.numberOfLines = 0;
    labText.textAlignment =NSTextAlignmentCenter;
    labText.backgroundColor = [UIColor clearColor];
    labText.text = MyLocalizedString(@"aboutInfo");
    [self.view addSubview:labText];

}

-(void)imageLogo
{
     //Log图片
    UIImageView *personView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"About1"]];
    personView.frame = CGRectMake((kScreenWidth-200)/2, 200, 200, 50);
    [self.view addSubview:personView];
    
    
    _labVersions = [[UILabel alloc]initWithFrame:CGRectMake((kScreenWidth-200)/2, 260, 200, 50)];
    _labVersions.backgroundColor = [UIColor clearColor];
    _labVersions.font = [UIFont systemFontOfSize:14];
    _labVersions.textAlignment  = NSTextAlignmentCenter;
    _labVersions.text =[self banben];
    _labVersions.textColor = [UIColor colorWithRed:132/255.0 green:132/255.0  blue:132/255.0  alpha:1];
    [self.view addSubview:_labVersions];
    
    UIButton *btn= [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.frame = CGRectMake((kScreenWidth-163)/2, (kScreenHeight-75-80-40-40), 163, 40);
    [btn setTitle:MyLocalizedString(@"newView") forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(Versionscheck) forControlEvents:UIControlEventTouchUpInside];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:@"btn100"] forState:UIControlStateNormal];
    [self.view addSubview:btn];
    
    
    UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake(0, kScreenHeight-60, kScreenHeight, 60)];
    view1.backgroundColor = [UIColor clearColor];
    [self.view addSubview:view1];
    
    UILabel *labWeiBo = [[UILabel alloc]initWithFrame:CGRectMake((kScreenWidth-160)/2, 0, 160, 25)];
    labWeiBo.backgroundColor = [UIColor clearColor];
    labWeiBo.font = [UIFont systemFontOfSize:12];
    labWeiBo.textAlignment = NSTextAlignmentCenter;
    labWeiBo.textColor = [UIColor colorWithRed:132/255.0 green:132/255.0  blue:132/255.0  alpha:1];
    labWeiBo.text = MyLocalizedString(@"labinfo1");
    [view1 addSubview:labWeiBo];
    
    
    UILabel *labWeiXin = [[UILabel alloc]initWithFrame:CGRectMake((kScreenWidth-160)/2, 20, 160, 25)];
    labWeiXin.backgroundColor = [UIColor clearColor];
    labWeiXin.textAlignment = NSTextAlignmentCenter;
    labWeiXin.font = [UIFont systemFontOfSize:12];
    labWeiXin.textColor = [UIColor colorWithRed:132/255.0 green:132/255.0  blue:132/255.0  alpha:1];
    labWeiXin.text = MyLocalizedString(@"labinfo2");
    [view1 addSubview:labWeiXin];
    
    
    


}


#pragma mark - 实现升级功能
-(void)Versionscheck
{
    [Loading showLoadingWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //执行耗时操作
        NSString *newVersion;
        //获取appId
        NSURL *url = [NSURL URLWithString:@"http://itunes.apple.com/lookup?id=692579125"];
        //通过url获取数据
        NSString *jsonResponseString =   [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
        //    NSLog(@"通过appStore获取的数据是：%@",jsonRes/ponseString);
        
        NSData *receiveData = [jsonResponseString dataUsingEncoding:NSUTF8StringEncoding];
        //解析json数据为数据字典
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:receiveData options:NSJSONReadingMutableLeaves error:nil];
        //从数据字典中检出版本号数据
        NSArray *configData = [dic valueForKey:@"results"];
        for(id config in configData)
        {
            newVersion = [config valueForKey:@"version"];
            _app_id  = [config valueForKey:@"trackId"];
        }
        NSLog(@"通过appStore获取的版本号是：%@",newVersion);
        //获取本地软件的版本号
        NSString *localVersion = [[[NSBundle mainBundle]infoDictionary] objectForKey:@"CFBundleVersion"];
        dispatch_async(dispatch_get_main_queue(), ^{
            //主线程更新UI
            [Loading hiddonLoadingWithView:self.view];
            
            //对比发现的新版本和本地的版本
            if ([newVersion floatValue] > [localVersion floatValue])
            {
                NSString *appStoreLink = [NSString stringWithFormat:@"http://itunes.apple.com/app/id%@",_app_id];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appStoreLink]];
            }
            else{
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:MyLocalizedString(@"alertinfo1") message:MyLocalizedString(@"alertinfo2") delegate:self cancelButtonTitle:MyLocalizedString(@"cancel") otherButtonTitles:MyLocalizedString(@"sure"), nil];
                [alert show];
            }
        });
    });
}



-(void)btnBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
