//
//  BookMarksViewController.m
//  MoreFunTV
//
//  Created by admin on 2014-11-9.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "BookMarksViewController.h"

@interface BookMarksViewController ()

@end

@implementation BookMarksViewController

-(id)initWithTitle:(NSString *)title strURL:(NSString *)strurl
{
    self = [super init];
    if (self) {
        _strTitle = title;
        _strUrl = strurl;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _seg.selectedSegmentIndex = 0;
    _isClick = YES;
    [_btnAdd setHidden:NO];
    NSMutableArray *arr = [DBDaoHelper selectCollectInfo];
    _arrCollect = [[NSMutableArray alloc]init];
    for (int i = arr.count-1; i >= 0; i--) {
        [_arrCollect addObject:[arr objectAtIndex:i]];
    }
    [_tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
    
    //自定义title
    [self creatNavigationView];
    
    //自定义tool
    [self creatToolbarView];
    
    //历史记录和收藏记录的列表
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 80, kScreenWidth, kScreenHeight-80-48) style:UITableViewStylePlain];
    _tableView.backgroundColor =[UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000];
    _tableView.delegate = self;
    _tableView.dataSource = self;
//    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.separatorColor = [UIColor colorWithRed:109.0/255.0 green:109.0/255.0 blue:109.0/255.0 alpha:1.000];//分割线颜色
    [self.view addSubview:_tableView];
}

//自定义的navigation
-(void)creatNavigationView
{
    UIView *naView = [[UIView alloc]init];
    naView.frame = CGRectMake(0, 0, kScreenWidth, 80);
    naView.backgroundColor = [UIColor colorWithWhite:0.297 alpha:1.000];
    [self.view addSubview:naView];
    
    //完成按钮
    UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnDone.frame = CGRectMake(0, 20, 60, 30);
    btnDone.backgroundColor = [UIColor clearColor];
    btnDone.titleLabel.font = [UIFont systemFontOfSize:17.0];
    [btnDone setTitle:MyLocalizedString(@"finish") forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(btnBack) forControlEvents:UIControlEventTouchUpInside];
    [naView addSubview:btnDone];
    
    //删除按钮按钮
    UIButton *btnDelete = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnDelete.frame = CGRectMake(kScreenWidth-60, 20, 60, 30);
    btnDelete.backgroundColor = [UIColor clearColor];
    btnDelete.titleLabel.font = [UIFont systemFontOfSize:17.0];
    [btnDelete setTitle:MyLocalizedString(@"delete") forState:UIControlStateNormal];
    [btnDelete setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnDelete addTarget:self action:@selector(btnDelete) forControlEvents:UIControlEventTouchUpInside];
    [naView addSubview:btnDelete];
    
    //标题
    _labTitle = [[UILabel alloc]init];
    _labTitle.frame = CGRectMake((kScreenWidth-80)/2, 20, 80, 30);
    _labTitle.backgroundColor =[UIColor clearColor];
    _labTitle.text = MyLocalizedString(@"title1");
    _labTitle.font = [UIFont systemFontOfSize:18.0];
    _labTitle.textAlignment = NSTextAlignmentCenter;
    _labTitle.textColor = [UIColor whiteColor];
    [naView addSubview:_labTitle];
    
    //选择控件
    NSArray *array = [[NSArray alloc]initWithObjects:MyLocalizedString(@"title1"),MyLocalizedString(@"title2") ,nil];
    _seg = [[UISegmentedControl alloc]initWithItems:array];
    _seg.frame = CGRectMake((kScreenWidth-240)/2, 50, 240, 25);
    _seg.tintColor = [UIColor colorWithRed:0.221 green:0.643 blue:0.969 alpha:1.000];
    [_seg addTarget:self action:@selector(selector:) forControlEvents:UIControlEventValueChanged];
    [naView addSubview:_seg];
}

//创建自定义toolbar
-(void)creatToolbarView
{
    UIView *toolView =[[UIView alloc]init];
    toolView.frame = CGRectMake(0, kScreenHeight-48, kScreenWidth, 48);
    toolView.backgroundColor = [UIColor colorWithWhite:0.216 alpha:0.700];
    [self.view addSubview:toolView];
    
    //编辑按钮
    UIButton *btnEdit = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnEdit.frame = CGRectMake(kScreenWidth-70, 4, 60, 40);
    btnEdit.backgroundColor = [UIColor clearColor];
    btnEdit.titleLabel.font = [UIFont systemFontOfSize:17.0];
    [btnEdit setTitle:MyLocalizedString(@"edit") forState:UIControlStateNormal];
    [btnEdit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnEdit addTarget:self action:@selector(btnEdit:) forControlEvents:UIControlEventTouchUpInside];
    [toolView addSubview:btnEdit];
    
    //添加按钮
    _btnAdd = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnAdd.frame = CGRectMake(10, 4, 60, 40);
    _btnAdd.backgroundColor = [UIColor clearColor];
    _btnAdd.titleLabel.font = [UIFont systemFontOfSize:17.0];
    [_btnAdd setTitle:@"添加" forState:UIControlStateNormal];
    [_btnAdd setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnAdd addTarget:self action:@selector(btnAdd) forControlEvents:UIControlEventTouchUpInside];
    [toolView addSubview:_btnAdd];
}

//完成按钮点击事件
-(void)btnBack
{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

//删除按钮
-(void)btnDelete{
    if (_isClick == YES) {
        _isAlter = 5678;
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:MyLocalizedString(@"alertinfo1") message:MyLocalizedString(@"alertinfo3") delegate:self cancelButtonTitle:MyLocalizedString(@"cancel") otherButtonTitles:MyLocalizedString(@"sure"), nil];
        [alert show];
    }
    else{
        _isAlter = 1011;
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:MyLocalizedString(@"alertinfo1") message:MyLocalizedString(@"alertinfo4") delegate:self cancelButtonTitle:MyLocalizedString(@"cancel") otherButtonTitles:MyLocalizedString(@"sure"), nil];
        [alert show];
    }
}

//编辑按钮点击事件
-(void)btnEdit:(UIButton *)sender
{
    //列表进入编辑状态
    [_tableView setEditing:!_tableView.editing animated:YES];
    if (_tableView.editing)
    {
        [sender setTitle:MyLocalizedString(@"cancel") forState:UIControlStateNormal];
    }
    else
    {
        [sender setTitle:MyLocalizedString(@"edit") forState:UIControlStateNormal];
    }
}

//添加按钮点击事件
-(void)btnAdd
{
    _isAlter = 1234;
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:MyLocalizedString(@"alertinfo1") message:MyLocalizedString(@"alertinfo5") delegate:self cancelButtonTitle:MyLocalizedString(@"cancel") otherButtonTitles:MyLocalizedString(@"sure"), nil];
    [alert show];
}

//清空历史记录
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        if (_isAlter == 1234) {
            HistoryInfo *history = [[HistoryInfo alloc]init];
            history.strTitle = _strTitle;
            history.strUrl = _strUrl;
            
            //将收藏记录存入数据库
            [DBDaoHelper insertCollectInfo:history];
            
            //查询收藏记录
            NSMutableArray *arr = [DBDaoHelper selectCollectInfo];
            _arrCollect = [[NSMutableArray alloc]init];
            for (int i = arr.count-1; i >= 0; i--) {
                [_arrCollect addObject:[arr objectAtIndex:i]];
            }
            [_tableView reloadData];
        }
        else if (_isAlter == 5678){
            [DBDaoHelper deleteAllCollecet];
            _arrCollect = [DBDaoHelper selectCollectInfo];
            [_tableView reloadData];
        }
        else if (_isAlter == 1011){
            [DBDaoHelper deleteAllHiistory];
            _arrMark = [DBDaoHelper selectWebHistory];
            [_tableView reloadData];
        }

    }
}

//seg点击事件
-(void)selector:(UISegmentedControl *)seg
{
    NSInteger index = seg.selectedSegmentIndex;
    if (index == 0)
    {
        _isClick = YES;
        _labTitle.text = MyLocalizedString(@"title1");
        NSMutableArray *arr = [DBDaoHelper selectCollectInfo];
        _arrCollect = [[NSMutableArray alloc]init];
        for (int i = arr.count-1; i >= 0; i--) {
            [_arrCollect addObject:[arr objectAtIndex:i]];
        }
        [_tableView reloadData];
        _btnAdd.hidden = NO;
    }
    else
    {
        _isClick = NO;
        _labTitle.text = MyLocalizedString(@"title2");
        NSMutableArray *arr = [DBDaoHelper selectWebHistory];
        _arrMark = [[NSMutableArray alloc]init];
        for (int i = arr.count-1; i >= 0; i--) {
            [_arrMark addObject:[arr objectAtIndex:i]];
        }
        [_tableView reloadData];
        _btnAdd.hidden = YES;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_isClick == YES) {
        return _arrCollect.count;
    }else{
        return _arrMark.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"cellmark";
    int row = [indexPath row];
    
    //获取cell row ＝ 0 获取不到 进入创建cell
    BookMarksTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil)
    {
        cell = [[BookMarksTableViewCell alloc]initWithStyle: UITableViewCellStyleDefault reuseIdentifier:identifier];
        [cell setBackgroundColor:[UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if (_isClick == YES) {
        HistoryInfo *hisCollect = [_arrCollect objectAtIndex:row];
        cell.labTiTle.text = hisCollect.strTitle;
        cell.labUrl.text = hisCollect.strUrl;
    }
    else{
        HistoryInfo *hisMark = [_arrMark objectAtIndex:row];
        cell.labTiTle.text = hisMark.strTitle;
        cell.labUrl.text = hisMark.strUrl;
    }

    return cell;
}

/*删除用到的函数*/
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //cell上的按钮  有插入 和 删除
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        if (_isClick == YES) {
            //删除数组中的数据
            HistoryInfo *hisCollect = [_arrCollect objectAtIndex:indexPath.row];
            [DBDaoHelper deleteCollecet:hisCollect];//删除数据库中的数据
            [_arrCollect removeObjectAtIndex:[indexPath row]];  //删除数组里的数据
        }
        else{
            HistoryInfo *hisMark = [_arrMark objectAtIndex:indexPath.row];
            [DBDaoHelper deleteHiistory:hisMark];;//删除数据库中的数据
            [_arrMark removeObjectAtIndex:[indexPath row]];  //删除数组里的数据
        }

        //重新加载cell  withRowAnimation为删除时的动画样式
        [_tableView deleteRowsAtIndexPaths:[NSMutableArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationMiddle];  //删除对应数据的cell
        [_tableView reloadData];
    }
}

//cell点击事件
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strWebUrl = @"";
    if (_isClick == YES) {
        HistoryInfo *hisCollect = [_arrCollect objectAtIndex:indexPath.row];
        strWebUrl = hisCollect.strUrl;
    }
    else{
        HistoryInfo *hisMark = [_arrMark objectAtIndex:indexPath.row];
        strWebUrl = hisMark.strUrl;
    }
    
    WebviewViewController *we = [[WebviewViewController alloc]initWithHtmlUrl:strWebUrl];
    [self presentViewController:we animated:YES completion:^{
    }];
}

/*改变删除按钮的title*/
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return MyLocalizedString(@"delete");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
