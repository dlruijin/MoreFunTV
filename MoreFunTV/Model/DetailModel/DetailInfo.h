//
//  DetailInfo.h
//  MoreFunTV
//
//  Created by admin on 2014-10-29.
//  Copyright (c) 2014年 admin. All rights reserved.
//
//  视频详情
//

#import <Foundation/Foundation.h>
#import "SeriesInfo.h"
@interface DetailInfo : NSObject

//影片详情页面标题
@property(nonatomic,retain) NSString * strTitleString;
//影片名
@property(nonatomic,retain) NSString * strTitle;
//影片的第几集-默认附值1
@property(nonatomic,retain) NSString * strIndex;
//影片的来源-默认附值空
@property(nonatomic,retain) NSString * strSource;
//类别
@property(nonatomic,retain) NSString * strCategoryId;
//图片
@property(nonatomic,retain) NSString * strThumbnail;
//剧集个数
@property(nonatomic,retain) NSString * strEpisodesCount;
//剧集的数组
@property(nonatomic,retain) NSMutableArray * arrAllPart;
//播放时间
@property(nonatomic,retain) NSDate * dateTime;
//观看至**分**秒
@property(nonatomic,retain) NSString * strHistoryTime;
//数据库表的主键
@property(nonatomic,retain) NSString * strPlayId;
//
@property(nonatomic,retain) NSString * strPlaySource;

@property(nonatomic,assign) int classId;
//资源ID
@property(nonatomic,retain) NSString * strResId;
//播放地址
@property(nonatomic,retain) NSString * strResUrl;

//投射网址
@property(nonatomic,retain) NSString * strRealUrl;
//投射的整集网址
@property(nonatomic,retain) NSString * strAllURL;
//Id
@property(nonatomic,retain) NSString * strId;

@property(nonatomic,retain) SeriesInfo *series;
@end
