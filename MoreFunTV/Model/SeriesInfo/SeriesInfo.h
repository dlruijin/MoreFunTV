//
//  SeriesInfo.h
//  MoreFunTV
//
//  Created by admin on 2014-10-29.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SeriesInfo : NSObject
@property(nonatomic,retain) NSString* strSeriesTitle;//剧集标题
@property(nonatomic,retain) NSString* strPlayUrl;//播放网址
@property(nonatomic,retain) NSString* strResId;//资源id
@end
