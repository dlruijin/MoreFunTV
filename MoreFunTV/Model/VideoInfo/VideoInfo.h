//
//  VideoInfo.h
//  MoreFunTV
//
//  Created by admin on 14-10-26.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoInfo : NSObject
@property (nonatomic,retain) NSString *strVideoId;//视频ID
@property (nonatomic,retain) NSString *strCategoryId;//分类ID
@property (nonatomic,retain) NSString *strCategoryName;//分类名
@property (nonatomic,retain) NSString *strTitle;//视频名称
@property (nonatomic,retain) NSString *strDescription;//视频描述
@property (nonatomic,retain) NSString *strThumbnail;//缩率图地址
@property (nonatomic,retain) NSString *strIsActive;//是否有效

@property (nonatomic,retain) NSString *strCategoryLog;//分类图标
@property (nonatomic,retain) NSString *strCategoryEn;//英文名

@property (nonatomic,retain) NSString *strReleasedEpisodes;//更新到
@end
