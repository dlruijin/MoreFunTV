//
//  SetUpUdf.h
//  MoreFunTV
//
//  Created by admin on 14-11-11.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SetUpUdf : NSObject
//是否保存历史记录
+(void)writeSetUpNSUdSaveHistory:(NSString *)strFlag;

//读取是否保存历史记录的标记
+(NSString *)readSetUpNSUdSaveHistoryFlag;

//是否保存播放记录
+(void)writeSetUpNSUdSavePlay:(NSString *)strFlag;

//读取是否保存播放记录的标记
+(NSString *)readSetUpNSUdSavePlayFlag;

//启用移动网络时通知我的标记写入
+(void)writeSetUpNSUdNetFlag:(NSString *)strFlag;

//启用移动网络时通知我的标记读取
+(NSString *)readSetUpNSUdNetFlag;

//消息通知的标记写入
+(void)writeSetUpNSUdInfoFlag:(NSString *)strFlag;

//消息通知的标记读取
+(NSString *)readSetUpNSUdInfoFlag;
@end
