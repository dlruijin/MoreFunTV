//
//  SetUpUdf.m
//  MoreFunTV
//
//  Created by admin on 14-11-11.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "SetUpUdf.h"

@implementation SetUpUdf
//是否保存历史记录
+(void)writeSetUpNSUdSaveHistory:(NSString *)strFlag
{
    NSUserDefaults *udShow = [NSUserDefaults standardUserDefaults];
    //存入之后 只要应用程序不关闭 哪儿都能用 只要程序不卸载 下次启动会继续存在
    [udShow setObject:strFlag forKey:@"HistorySave"];
    //写入
    [udShow synchronize];
}

//读取是否保存历史记录的标记
+(NSString *)readSetUpNSUdSaveHistoryFlag
{
    NSUserDefaults *udShow = [NSUserDefaults standardUserDefaults];
    return [udShow objectForKey:@"HistorySave"];
}

//是否保存播放记录
+(void)writeSetUpNSUdSavePlay:(NSString *)strFlag
{
    NSUserDefaults *udShow = [NSUserDefaults standardUserDefaults];
    //存入之后 只要应用程序不关闭 哪儿都能用 只要程序不卸载 下次启动会继续存在
    [udShow setObject:strFlag forKey:@"PalySave"];
    //写入
    [udShow synchronize];
}

//读取是否保存播放记录的标记
+(NSString *)readSetUpNSUdSavePlayFlag
{
    NSUserDefaults *udShow = [NSUserDefaults standardUserDefaults];
    return [udShow objectForKey:@"PalySave"];
}

//启用移动网络时通知我的标记写入
+(void)writeSetUpNSUdNetFlag:(NSString *)strFlag
{
    NSUserDefaults *udShow = [NSUserDefaults standardUserDefaults];
    //存入之后 只要应用程序不关闭 哪儿都能用 只要程序不卸载 下次启动会继续存在
    [udShow setObject:strFlag forKey:@"Net"];
    //写入
    [udShow synchronize];
}

//启用移动网络时通知我的标记读取
+(NSString *)readSetUpNSUdNetFlag
{
    NSUserDefaults *udShow = [NSUserDefaults standardUserDefaults];
    return [udShow objectForKey:@"Net"];
}

//消息通知的标记写入
+(void)writeSetUpNSUdInfoFlag:(NSString *)strFlag
{
    NSUserDefaults *udShow = [NSUserDefaults standardUserDefaults];
    //存入之后 只要应用程序不关闭 哪儿都能用 只要程序不卸载 下次启动会继续存在
    [udShow setObject:strFlag forKey:@"INFO"];
    //写入
    [udShow synchronize];
}

//消息通知的标记读取
+(NSString *)readSetUpNSUdInfoFlag
{
    NSUserDefaults *udShow = [NSUserDefaults standardUserDefaults];
    return [udShow objectForKey:@"INFO"];
}
@end
