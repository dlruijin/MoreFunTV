//
//  HotWords.h
//  MoreFunTV
//
//  Created by admin on 14-10-27.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotWords : NSObject
@property (nonatomic,retain) NSString *strHotWordsId;//热词ID
@property (nonatomic,retain) NSString *strHotWordsName;//热词名
@end
