//
//  NetWorkingHelper.h
//  MoreFunTV
//
//  Created by admin on 14-10-30.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetWorking.h"
#import "VideoInfo.h"
#import "LiveInfo.h"
#import "Search.h"
#import "HotWords.h"
#import "HistoryInfo.h"
#import "Client.h"
#import "Subapps.h"
#import "DBDaoHelper.h"
#import "DetailInfo.h"


@interface NetWorkingHelper : NSObject
//获取推荐信息数据
+ (NSMutableArray *)getRecommendInfoWithRequestParameter:(RequestParameter *)reqParameter;

//获取单个分类的详细信息
+ (NSMutableArray *)getCategoryVoideInfoWithRequestParameter:(RequestParameter *)reqParameter;

//获取直播信息数据
+ (NSMutableArray *)getLiveInfoWithRequestParameter:(RequestParameter *)reqParameter;

//获取直播EPG信息数据 并存入数据库
+(void)getDetailEPGInfoWith:(RequestParameter *)rep ArrLive:(NSMutableArray *)arrLive;

//点击按钮搜索 请求数据
+ (NSMutableArray *)getSearchBtnWithRep:(RequestParameter *)reqParameter;

//联想搜索 请求数据
+ (NSMutableArray *)getSearchWithWords:(NSString *)words;

//热词查询详情
+ (NSMutableArray *)getHotWordsWithRequestParameter:(RequestParameter *)reqParameter;

//获取类别信息
+ (NSMutableArray *)getClassInfoWith:(NSString *)name;

//获取类别下的条件选项信息
+ (NSMutableArray *)getTypeInfoWithTypeName:(NSString *)name;

//获取客户端信息
+ (Client *)getClientInfoWithRep:(RequestParameter *)reqParameter;

//获取根应用信息
+ (NSMutableArray *)getSubappsInfoWithRep:(RequestParameter *)reqParameter;

//获取根应用下的子应用详细信息
+ (NSMutableArray *)getSubappsDeailWithRep:(RequestParameter *)reqParameter;

//获取视频资源详细信息
+ (NSMutableArray *)getDetailResourceInfomationRequestParameter:(RequestParameter *)reqParameter;


@end
