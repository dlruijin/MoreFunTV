//
//  AppendingURL.h
//  MoreFunTV
//
//  Created by admin on 14-10-25.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SafeCheckParameter.h"
#import "RequestParameter.h"
#import "SetUpSingleton.h"

@interface AppendingURL : NSObject
//网络请求网址拼接网址拼接
+ (NSString *)strAppendingURLWith:(RequestParameter *)reqParameter;

//获取单个客户端  获取客户端的一级应用列表
+ (NSString *)strAppendingClientURLWith:(RequestParameter *)reqParameter;

@end
