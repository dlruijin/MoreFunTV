//
//  MoreFunTV
//
//  Created by admin on 14-10-25.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "AppendingURL.h"

@implementation AppendingURL
//网络请求网址拼接
+ (NSString *)strAppendingURLWith:(RequestParameter *)reqParameter
{
    //单例
    SetUpSingleton *set = [SetUpSingleton sharedSetUp];
    if (set.strApiURL.length == 0) {
        set.strApiURL = apiURL;
    }
    
//    //暂时替代
//    set.strApiURL = apiURL;
    
    NSString *strRequestURL = @"";
    if (reqParameter.portID == 1)
    {//直播列表数据请求
        NSString *strURL = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",set.strApiURL,resources,categoryID,areaID,styleID,pageNo,pageSize,[SafeCheckParameter safeCheckParameterAppending]];
        strRequestURL = [NSString stringWithFormat:strURL,reqParameter.strCategoryID,reqParameter.strAreaID,reqParameter.strStyleID,reqParameter.strPageNo,reqParameter.strPageSize];
    }
    else if (reqParameter.portID == 2)
    {//获取部分资源信息数据页
        NSString *strURL = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@",set.strApiURL,resources,categoryID,areaID,styleID,pageNo,pageSize,releasedOnFrom,releasedOnTo,[SafeCheckParameter safeCheckParameterAppending]];
        strRequestURL = [NSString stringWithFormat:strURL,reqParameter.strCategoryID,reqParameter.strAreaID,reqParameter.strStyleID,reqParameter.strPageNo,reqParameter.strPageSize,reqParameter.strReleasedOnFrom,reqParameter.strReleasedOnTo];
    }
    else if (reqParameter.portID == 3)
    {//获取推荐信息列表(接口中的信息是所全部的视频信息)
        NSString *strURL = [NSString stringWithFormat:@"%@%@%@%@%@%@",set.strApiURL,resources,appID,pageNo,pageSize,[SafeCheckParameter safeCheckParameterAppending]];
        strRequestURL = [NSString stringWithFormat:strURL,reqParameter.strAppID,reqParameter.strPageNo,reqParameter.strPageSize];
    }
    else if (reqParameter.portID == 4)
    {//热词查询接口数据
        NSString *strURL = [NSString stringWithFormat:@"%@%@%@%@%@",set.strApiURL,hotWords,pageNo,pageSize,[SafeCheckParameter safeCheckParameterAppending]];
        strRequestURL = [NSString stringWithFormat:strURL,reqParameter.strPageNo,reqParameter.strPageSize];
    }
    else if (reqParameter.portID == 5)
    {//搜索接口数据
        NSString *strURL = [NSString stringWithFormat:@"%@%@%@%@%@%@",set.strApiURL,searchRe,categoryID,pageNo,pageSize,[SafeCheckParameter safeCheckParameterAppending]];
        strRequestURL = [NSString stringWithFormat:strURL,reqParameter.strSearchWords,reqParameter.strCategoryID,reqParameter.strPageNo,reqParameter.strPageSize];
    }
    else if (reqParameter.portID == 6)
    {//历史数据接口
        NSString *strURL = [NSString stringWithFormat:@"%@%@%@%@%@%@",set.strApiURL,historyRecord,pageNo,pageSize,appID,[SafeCheckParameter safeCheckParameterAppending]];
        strRequestURL = [NSString stringWithFormat:strURL,reqParameter.strPageNo,reqParameter.strPageSize,reqParameter.strAppID];
    }
    else if(reqParameter.portID == 7)
    {//详细资源
        NSString *strURL = [NSString stringWithFormat:@"%@%@%@%@%@",set.strApiURL,detailResource,pageSize,pageNo,[SafeCheckParameter safeCheckParameterAppending]];
        
        strRequestURL = [NSString stringWithFormat:strURL,reqParameter.strResId,reqParameter.strPageSize,reqParameter.strPageNo,reqParameter.strAppID];
    }
    else if(reqParameter.portID == 8)
    {//获取直播的RPG信息
        NSString *strURL = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",set.strApiURL,EPG,Method,startOnFrom,startOnTo,pageSize,pageNo,[SafeCheckParameter safeCheckParameterAppending]];
        
        strRequestURL = [NSString stringWithFormat:strURL,reqParameter.strResId,reqParameter.strStartOnFrom,reqParameter.strStartOnTo,reqParameter.strPageSize,reqParameter.strPageNo,reqParameter.strAppID];
    }
    return strRequestURL;
}

//获取单个客户端以及获取客户端的一级应用列表
+ (NSString *)strAppendingClientURLWith:(RequestParameter *)reqParameter
{
    //单例
    SetUpSingleton *set = [SetUpSingleton sharedSetUp];
    if (set.strApiURL.length == 0) {
        set.strApiURL = apiURL;
    }
    
//    //暂时替代
//    set.strApiURL = apiURL;
    
    NSString *strRequestURL = @"";
    if ([reqParameter.strKeyWord isEqual:@"clients"])
    {
        NSString *strURL = [NSString stringWithFormat:@"%@%@%@",set.strApiURL,clients,[SafeCheckParameter safeCheckParameterAppending]];
        strRequestURL = [NSString stringWithFormat:strURL,cID];
    }
    else if ([reqParameter.strKeyWord isEqual:@"subapps"])
    {
        NSString *strURL = [NSString stringWithFormat:@"%@%@%@%@",set.strApiURL,subapps,pageSize,[SafeCheckParameter safeCheckParameterAppending]];
        strRequestURL = [NSString stringWithFormat:strURL,reqParameter.strRootAppId,reqParameter.strPageSize];
    }
    return strRequestURL;
}
@end

