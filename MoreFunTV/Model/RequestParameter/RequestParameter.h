//
//  RequestParameter.h
//  MoreFunTV
//
//  Created by admin on 14-10-30.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestParameter : NSObject
//接口标记
@property (nonatomic,assign) int portID;
/*
 //直播列表数据请求                            portID = 1
 //获取部分资源信息数据页                       portID = 2
 //获取推荐信息列表(接口中的信息是所全部的视频信息) portID = 3
 //热词查询接口数据                            portID = 4
 //搜索接口数据                               portID = 5
 //历史数据接口                               portID = 6
 //获取EPG信息                                portID = 8
 */

//请求参数
//分类ID
@property (nonatomic,retain) NSString *strCategoryID;
//地区ID
@property (nonatomic,retain) NSString *strAreaID;
//风格ID
@property (nonatomic,retain) NSString *strStyleID;
@property (nonatomic,retain) NSString *strPageNo;//页码
//条数
@property (nonatomic,retain) NSString *strPageSize;
//appID
@property (nonatomic,retain) NSString *strAppID;
//起始时间
@property (nonatomic,retain) NSString *strReleasedOnFrom;
//截止时间
@property (nonatomic,retain) NSString *strReleasedOnTo;
//搜索热词
@property (nonatomic,retain) NSString *strSearchWords;
//安全码
@property (nonatomic,retain) NSString *strKeyWord;
//根ID
@property (nonatomic,retain) NSString *strRootAppId;
//资源地址ID
@property (nonatomic,retain) NSString *strResId;
//EPG ID
@property (nonatomic,retain) NSString *strEPGResId;
//开始
@property (nonatomic,retain) NSString *strStartOnFrom;
//截止
@property (nonatomic,retain) NSString *strStartOnTo;
//标记
@property (nonatomic,assign) int flag;
//页码标记
@property (nonatomic,assign) int flagPage;

@end
