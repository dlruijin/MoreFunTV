//
//  SafeCheckParameter.m
//  MoreFunTV
//
//  Created by admin on 14-10-25.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "SafeCheckParameter.h"

@implementation SafeCheckParameter

//安全校验参数拼接
+(NSString *)safeCheckParameterAppending
{
    NSString *strUserId = @"0";
    NSString *strSessiomId = @"dltianwei";
    NSString *strTimeTamp = [self timeSpCalculate];
    NSString *strEncryptKey = @"65c3c2a7152c4a5986143996b2a8a564";
    NSString *strMd5 = [NSString stringWithFormat:@"%@%@%@%@",strUserId,strSessiomId,strTimeTamp,strEncryptKey];
    //加密
    NSString *strSecurityCode = [self md5:strMd5];
    //拼接安全校验参数
    NSString *strSafeCheckParameter = [NSString stringWithFormat:safeCheckParameter,strTimeTamp,strSecurityCode];
    return strSafeCheckParameter;
}

//时间戳计算
+(NSString *)timeSpCalculate
{
    //获取当前时间
    NSDate *datenow = [[NSDate alloc]init];
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]];
    return timeSp;
}

//MD5加密算法
+(NSString *)md5:(NSString *)str
{
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, strlen(cStr), result); // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}
@end
