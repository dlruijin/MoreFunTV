//
//  Subapps.h
//  MoreFunTV
//
//  Created by admin on 14-10-30.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Subapps : NSObject
@property (nonatomic,retain) NSString *strSubappId;//一级应用ID 子应用ID
@property (nonatomic,retain) NSString *strSubappName;//一级应用名 子应用名
@property (nonatomic,retain) NSString *strDescription;//概述
@property (nonatomic,retain) NSString *strSubappImageUrl;//大图推荐图片地址
@property (nonatomic,retain) NSString *strSubappThumbnail;//图片地址
@end
