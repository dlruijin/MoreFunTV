//
//  DBDaoHelper.h
//  MoreFunTV
//
//  Created by admin on 14-11-7.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBHelper.h"
#import "Search.h"
#import "HistoryInfo.h"
#import "FMDatabase.h"
#import "LiveInfo.h"
#import "DetailInfo.h"
#import "XiangQingInfo.h"

@interface DBDaoHelper : NSObject
//创建数据库表
+(BOOL)createAllTable;

//插入历史记录信息
+(BOOL )insertSearchHistory:(Search *)searchInfo;

//插入收藏列表信息
+(BOOL)insertCollectInfo:(HistoryInfo *)history;

//插入网页历史记录信息
+(BOOL)insertWebHistory:(HistoryInfo *)history;

//插入直播Id信息
+(BOOL)insertLiveIDWith:(LiveInfo *)live;

//插入EPG信息
+(BOOL)insertEPGWith:(LiveInfo *)live;

//更新播放记录信息
+(BOOL)updatePlay:(DetailInfo *)detail;

//更新详情选择记录信息
+(BOOL)updateXiangQing:(XiangQingInfo *)detail;

//插入播放记录信息
+(BOOL)insertPlay:(DetailInfo *)detail;

//插入详情记录信息
+(BOOL)insertXiangQing:(XiangQingInfo *)detail;

//插入投射记录信息
+(BOOL)insertTouShe:(DetailInfo *)detail;

//查询历史记录信息
+(NSMutableArray *)selectSearchHistory;

//查询收藏列表信息
+(NSMutableArray *)selectCollectInfo;

//查询最后加入的一条网页历史记录信息
+(HistoryInfo *)selectLastWebHistory;

//查询网页历史记录信息
+(NSMutableArray *)selectWebHistory;

//查询直播的ID
+(NSMutableArray *)selectLiveID:(int)flag;

//查询最新播放记录的play_id
+(NSString *)selectStrPlayId;

//根据play_res_id查询详情记录的最近一条信息
+(XiangQingInfo *)selectStrResId:(NSString *)strResId;

//查询所有的播放记录信息
+(NSMutableArray *)selectAllPlayInfo;

//查询正在播放的EPG信息
+(LiveInfo *)selectNowEpgInfo:(LiveInfo *)live;

//查询所有的EPG信息
+(LiveInfo *)selectAllEpgInfo:(LiveInfo *)live;

//查询投射信息是否已经插入
+(BOOL)selectTouShe:(NSString *)strTitle;

//查询所有的投射记录信息
+(NSMutableArray *)selectAllTouShe;

//删除第一条历史记录信息
+(void)deleteSearchHistoryMin;

//删除全部历史记录信息
+(void)deleteSearchHistoryAll;

//删除一条收藏列表记录
+(void)deleteCollecet:(HistoryInfo *)history;

//删除一条网页历史记录
+(void)deleteHiistory:(HistoryInfo *)history;

//删除全部收藏列表记录
+(void)deleteAllCollecet;

//删除全部网页历史记录
+(void)deleteAllHiistory;

//删除全部LiveID信息
+(void)deleteAllLiveID;

//删除全部EPG信息
+(void)deleteAllEPG;

//删除全部播放记录信息
+(void)deleteAllTABLEPlay;

//删除一条投射记录
+(void)deleteTouShe:(DetailInfo *)deta;

//删除全部投射记录
+(void)deleteAllTouShe;
@end
