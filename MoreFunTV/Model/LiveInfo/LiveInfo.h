//
//  LiveInfo.h
//  MoreFunTV
//
//  Created by admin on 14-10-26.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LiveInfo : NSObject
@property (nonatomic,retain) NSString *strLiveId;//直播ID
@property (nonatomic,retain) NSString *strLiveTime;//时间戳
@property (nonatomic,retain) NSString *strPlayUrl;//播放地址
@property (nonatomic,assign) int flag;//标记

@property (nonatomic,retain) NSString *strStartOn;//开始时间
@property (nonatomic,retain) NSString *strId;//节目ID
@property (nonatomic,retain) NSString *strNextId;//下一个节目ID
@property (nonatomic,retain) NSString *strTitle;//节目名称
@property (nonatomic,retain) NSString *strNextTitle;//下一个节目的名称
@property (nonatomic,retain) NSString *strEndOn;//结束时间
@property (nonatomic,retain) NSString *strThumbnail;//缩率图地址
@property (nonatomic,retain) NSString *strResId;//直播资源ID
@property (nonatomic,retain) NSString *strResTitle;//直播名称


@end
