//
//  Search.h
//  MoreFunTV
//
//  Created by admin on 14-10-27.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Search : NSObject
//搜库数据  资源搜索接口
@property (nonatomic,retain) NSString *strId;//视频ID
@property (nonatomic,retain) NSString *strTitle;//视频名称
@property (nonatomic,retain) NSString *strCategoryName;//类型名
@property (nonatomic,retain) NSString *strThumbnail;//图片地址
@property (nonatomic,retain) NSString *strDescription;//描述
@property (nonatomic,assign) int flag;//标记
@end
