//
//  PlayControlView.m
//  MoreFunTV
//
//  Created by admin on 14-11-3.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "PlayControlView.h"

@implementation PlayControlView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        //圆形的底部图片
        UIImageView *imgBottom = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 216, 216)];
        [imgBottom setImage:[UIImage imageNamed:@"disc.png"]];
        [imgBottom setUserInteractionEnabled:YES];
        [self addSubview:imgBottom];
        
        //播放按钮底部图片
        UIImageView *imgStart = [[UIImageView alloc]initWithFrame:CGRectMake((216-74)/2, (216-74)/2, 74, 74)];
        [imgStart setImage:[UIImage imageNamed:@"start.png"]];
        [imgStart setUserInteractionEnabled:YES];
        [imgBottom addSubview:imgStart];
        
        //播放按钮
        self.btnPlay = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        self.btnPlay.frame = CGRectMake((216-72)/2, (216-72)/2, 72, 72);
        self.btnPlay.tag = 10455;
        self.btnPlay.tintColor = [UIColor whiteColor];
        [self.btnPlay setImage:[UIImage imageNamed:@"playplay.png"] forState:UIControlStateNormal];
        [self.btnPlay setBackgroundColor:[UIColor clearColor]];
        self.btnPlay.layer.borderWidth = 1.0;
        self.btnPlay.layer.borderColor = [[UIColor clearColor]CGColor];
        self.btnPlay.layer.cornerRadius = 36.0;
        self.btnPlay.clipsToBounds = YES;
        [self.btnPlay addTarget:self action:@selector(clickPlay:) forControlEvents:UIControlEventTouchUpInside];
        [imgBottom addSubview:self.btnPlay];
        
        //音量＋
        UIButton *btnPlus = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        btnPlus.frame = CGRectMake((216-46)/2, 5, 46, 46);
        btnPlus.tag = 10222;
        btnPlus.tintColor = [UIColor whiteColor];
        [btnPlus setImage:[UIImage imageNamed:@"jia.png"] forState:UIControlStateNormal];
        [btnPlus setBackgroundColor:[UIColor clearColor]];
        [btnPlus addTarget:self action:@selector(clickPlay:) forControlEvents:UIControlEventTouchUpInside];
        [imgBottom addSubview:btnPlus];
        
        //音量－
        UIButton *btnSubtract = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        btnSubtract.frame = CGRectMake((216-46)/2, 216-5-46, 46, 46);
        btnSubtract.tag = 10112;
        btnSubtract.tintColor = [UIColor whiteColor];
        [btnSubtract setImage:[UIImage imageNamed:@"jian.png"] forState:UIControlStateNormal];
        [btnSubtract setBackgroundColor:[UIColor clearColor]];
        [btnSubtract addTarget:self action:@selector(clickPlay:) forControlEvents:UIControlEventTouchUpInside];
        [imgBottom addSubview:btnSubtract];
        
        //上一个
        UIButton *btnForward = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        btnForward.frame = CGRectMake(5, (216-46)/2, 46, 46);
        btnForward.tag = 10337;
        btnForward.tintColor = [UIColor whiteColor];
        [btnForward setImage:[UIImage imageNamed:@"fowordqian.png"] forState:UIControlStateNormal];
        [btnForward setBackgroundColor:[UIColor clearColor]];
        [btnForward addTarget:self action:@selector(clickPlay:) forControlEvents:UIControlEventTouchUpInside];
        [imgBottom addSubview:btnForward];
        
        //下一个
        UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        btnNext.frame = CGRectMake(216-5-46, (216-46)/2, 46, 46);
        btnNext.tag = 10657;
        btnNext.tintColor = [UIColor whiteColor];
        [btnNext setImage:[UIImage imageNamed:@"nexthou.png"] forState:UIControlStateNormal];
        [btnNext setBackgroundColor:[UIColor clearColor]];
        [btnNext addTarget:self action:@selector(clickPlay:) forControlEvents:UIControlEventTouchUpInside];
        [imgBottom addSubview:btnNext];
        
    }
    return self;
}

-(void)clickPlay:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(playControlView:ButtonControl:)]) {
        [self.delegate playControlView:self ButtonControl:sender];
    }
}

-(void)changBtnImge:(BOOL)flag{
    if (flag == YES) {
        [self.btnPlay setImage:[UIImage imageNamed:@"playfilm.png"] forState:UIControlStateNormal];
    }
    else{
        [self.btnPlay setImage:[UIImage imageNamed:@"playplay.png"] forState:UIControlStateNormal];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
