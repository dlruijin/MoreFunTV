//
//  ChooseView.m
//  MoreFunTV
//
//  Created by admin on 14-10-22.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "ChooseView.h"

@implementation ChooseView

- (id)initWithCondition:(NSMutableArray *)arrCondition Button:(UIButton *)btn
{
    self = [super init];
    if (self) {
        self.arrCondition = arrCondition;
        self.btn = btn;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.clipsToBounds = YES;
        
        _tableVideo = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth,40*4) style:UITableViewStylePlain];
        _tableVideo.separatorColor = [UIColor colorWithWhite:0.542 alpha:1.000];//分割线颜色
        _tableVideo.showsVerticalScrollIndicator = YES;//隐藏纵向滚动条
        _tableVideo.delegate = self;
        _tableVideo.dataSource = self;
        _tableVideo.bounces = NO;
        [_tableVideo setBackgroundColor:[UIColor clearColor]];
        [self addSubview:_tableVideo];
        
        UIView *squareView = [[UIView alloc]initWithFrame:CGRectMake(0, 40*4, kScreenWidth, kScreenHeight-40*4)];
        [squareView setBackgroundColor:[UIColor clearColor]];
        [self addSubview:squareView];
        
        //创建点击手势对象
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
        //设置点击次数
        tapRecognizer.numberOfTapsRequired = 1;
        //关键语句，给self.view添加一个点击手势监测；
        [squareView addGestureRecognizer:tapRecognizer];
    }
    return self;
}

//点击事件
-(void)tapClick:(UITapGestureRecognizer *)sender
{
    if ([self.delegate respondsToSelector:@selector(tapRecognizerChooseView:Flag:Button:)]) {
        [self.delegate tapRecognizerChooseView:self Flag:YES Button:self.btn];
    }
}

//行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.arrCondition.count < 5)
    {
        _tableVideo.frame = CGRectMake(0, 0, kScreenWidth,40*self.arrCondition.count);
    }
    return self.arrCondition.count;
}

//列表每行的高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

//列表每行显示的内容
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"firstTable";
    //row 代表列表的那一行
    int row = [indexPath row];
    
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle: UITableViewCellStyleDefault reuseIdentifier:identifier];
    [cell setBackgroundColor:[UIColor whiteColor]];
    cell.textLabel.font = [UIFont systemFontOfSize:17.0f];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"123"] highlightedImage:[UIImage imageNamed:@"flag.png"]];
    imageView.frame = CGRectMake(kScreenWidth-44 , 12, 20, 15);
    [cell addSubview:imageView];
    
    VideoInfo *videoInfo = [self.arrCondition objectAtIndex:row];
    cell.textLabel.text = videoInfo.strCategoryName;
    
    if ([self.btn.titleLabel.text isEqual:videoInfo.strCategoryName])
    {
        cell.textLabel.textColor = [UIColor colorWithRed:63.0/255.0 green:169.0/255.0 blue:245.0/255.0 alpha:1.000];
        [imageView setImage:[UIImage imageNamed:@"flag.png"]];
    }
    if ([self.btn.titleLabel.text isEqual:MyLocalizedString(@"type1")] || [self.btn.titleLabel.text isEqual:MyLocalizedString(@"type3")] || [self.btn.titleLabel.text isEqual:MyLocalizedString(@"type2")]) {
        if (row == 0){
            cell.textLabel.textColor = [UIColor colorWithRed:63.0/255.0 green:169.0/255.0 blue:245.0/255.0 alpha:1.000];
            [imageView setImage:[UIImage imageNamed:@"flag.png"]];
        }
    }
    return cell;
}

//cell点击事件
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    VideoInfo *videoInfo = [self.arrCondition objectAtIndex:indexPath.row];
    if ([self.delegate respondsToSelector:@selector(chooseView:Condition:Button:)])
    {
        [self.delegate chooseView:self Condition:videoInfo Button:self.btn];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
